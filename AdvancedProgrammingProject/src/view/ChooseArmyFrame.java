package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;

import javax.swing.JButton;

import model.dto.Army;
import model.Game;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class ChooseArmyFrame extends JFrame {

	private JPanel contentPane;
	private JButton[] btns;
	private JLabel[] lbls;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public ChooseArmyFrame(Game game, List<Army> armies) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		lbls = new JLabel[armies.size()];
		btns = new JButton[armies.size()];
		for (int i = 0; i < armies.size(); i++) {
			lbls[i] = new JLabel("Player " + armies.get(i).getOwner().getName() + "'s army. ");
			lbls[i].setForeground(armies.get(i).getOwner().getColor());
			GridBagConstraints gbc_lblNazwaArmii = new GridBagConstraints();
			gbc_lblNazwaArmii.insets = new Insets(0, 0, 0, 5);
			gbc_lblNazwaArmii.gridx = 0;
			gbc_lblNazwaArmii.gridy = i;
			contentPane.add(lbls[i], gbc_lblNazwaArmii);

			btns[i] = new JButton("Destroy");
			GridBagConstraints gbc_btnChoose = new GridBagConstraints();
			gbc_btnChoose.gridx = 1;
			gbc_btnChoose.gridy = i;
			btns[i].addActionListener(new ChooseActionListener(game, armies.get(i), this));
			contentPane.add(btns[i], gbc_btnChoose);
		}
	}

	class ChooseActionListener implements ActionListener {
		private final Army army;
		private final Game game;
		private final ChooseArmyFrame frame;

		ChooseActionListener(final Game game, final Army army,
				ChooseArmyFrame frame) {
			super();
			this.game = game;
			this.frame = frame;
			this.army = army;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			game.chooseArmy(army);
			frame.dispose();
		}
	}

}
