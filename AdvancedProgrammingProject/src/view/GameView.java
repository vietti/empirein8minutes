package view;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Scanner;

import model.Game;
import model.dto.ActionName;
import model.dto.Army;
import model.dto.BidEntry;
import model.dto.Card;
import model.dto.CardType;
import model.dto.Player;
import model.dto.Territory;
import model.state.ArmyChosenState;
import model.state.BeforeChoosingCardState;
import model.state.BiddingState;
import model.state.CardChosenState;
import model.state.StartGameState;
import model.state.State;
import model.state.TerritoryChosenState;

public class GameView implements View {

	Game game;
	Scanner scanner = new Scanner(System.in);

	public GameView(Game game) {
		this.game = game;
		game.registerView(this);
	}

	@Override
	public void update(Observable o, Object arg) {
		State state = game.getState();
		if (state instanceof StartGameState) {
			if (game.isGameFinished()) {
				showResultsView();
			} else
				showStartGameView();
		} else if (state instanceof BiddingState) {
			showBiddingView();
		} else if (state instanceof BeforeChoosingCardState) {
			showCardsView();
		} else if (state instanceof CardChosenState) {
			// TODO: second
			Card card = game.getCurrentCard();
			if (CardType.TO_CHOOSE.equals(card.getType())) {
				showActionsView();
			}
			switch (card.getActionName()) {
			case DESTROY_ARMY:
				showActionInformation();
				showArmiesView();
				break;
			case MOVE_ARMY:
				showActionInformation();
				showArmiesView();
				break;
			case MOVE_ARMY_BY_SEA:
				showActionInformation();
				showArmiesView();
				break;
			case PLACE_CITY:
				showActionInformation();
				showTerritoriesView();
				break;
			case PLACE_ARMY:
				showActionInformation();
				showTerritoriesView();
				break;
			}
		} else if (state instanceof ArmyChosenState) {
			Card card = game.getCurrentCard();
			switch (card.getActionName()) {
			case MOVE_ARMY:
				showActionInformation();
				showTerritoriesView();
				break;
			case MOVE_ARMY_BY_SEA:
				showActionInformation();
				showTerritoriesView();
				break;
			case DESTROY_ARMY:
				showArmiesView();
				break;
			default:
				System.err.println("Impossible: " + card.getActionName() + ", "
						+ card.getType());
				break;
			}
		} else if (state instanceof TerritoryChosenState) {
			Card card = game.getCurrentCard();

			switch (card.getActionName()) {
			case MOVE_ARMY:
				showActionInformation();
				showArmiesView();
				break;
			case MOVE_ARMY_BY_SEA:
				showActionInformation();
				showArmiesView();
				break;
			case PLACE_CITY:
				showActionInformation();
				showTerritoriesView();
				break;
			case PLACE_ARMY:
				showActionInformation();
				showTerritoriesView();
				break;
			default:
				System.err.println("Impossible: " + card.getActionName() + ", "
						+ card.getType());
				break;
			}
		}

	}

	public int getCard() {
		int number = 0;
		Card[] cards = game.getAllowedCards();
		while (number < 1 || number > cards.length) {
			printMessage("Choose card from cards given in top.");
			printMessage("Enter the number of card:");
			while (!scanner.hasNextInt()) {
				printMessage(scanner.next() + " is not numerical value!");
				printMessage("Enter the number of card:");
			}
			number = scanner.nextInt();
		}

		return number - 1;
	}

	public Army getArmy() {
		int number = 0;
		List<Army> armies = game.getAllowedArmies();
		while (number < 1 || number > armies.size()) {
			printMessage("Choose army from armies given in top.");
			printMessage("Enter the number of army:");
			while (!scanner.hasNextInt()) {
				printMessage(scanner.next() + " is not numerical value!");
				printMessage("Enter the number of army:");
			}
			number = scanner.nextInt();
		}

		return armies.get(number - 1);
	}

	public ActionName getAction() {
		int number = 0;
		while (number < 1 || number > 2) {
			printMessage("Choose action from actions given in top.");
			printMessage("Enter the number of action:");
			while (!scanner.hasNextInt()) {
				printMessage(scanner.next() + " is not numerical value!");
				printMessage("Enter the number of action:");
			}
			number = scanner.nextInt();
		}
		Card card = game.getCurrentCard();
		if (number == 1) {
			return card.getActionName();
		} else {
			return card.getSecondActionName();
		}
	}

	public Territory getTerritory() {
		int number = 0;
		List<Territory> territories = game.getAllowedTerritories();
		while (number < 1 || number > territories.size()) {
			printMessage("Choose territory from territories given in top.");
			printMessage("Enter the number of territory (number before dot):");
			while (!scanner.hasNextInt()) {
				printMessage(scanner.next() + " is not numerical value!");
				printMessage("Enter the number of territory (number before dot):");
			}
			number = scanner.nextInt();
		}
		return territories.get(number - 1);
	}

	public Entry<String, Integer> getBid() {
		printMessage("Enter the name of player:");
		String name = null;
		int bidValue = -1;
		while (!scanner.hasNext()) {
			printMessage("Enter the name of player:");
		}
		name = scanner.next();
		while (bidValue < 0) {
			printMessage("Bid should be numerical value bigger than 0.");
			printMessage("Enter bid for player " + name + ":");
			while (!scanner.hasNextInt()) {
				printMessage(scanner.next() + "is not numerical value!");
				printMessage("Enter bid for player " + name + ":");
			}
			bidValue = scanner.nextInt();
		}
		Entry<String, Integer> bid = new BidEntry<String, Integer>(name,
				bidValue);
		return bid;
	}

	public int getNumberOfPlayers() {
		int number = 0;
		while (number < 2 || number > 4) {
			printMessage("Number of players should been numerical value beetwen 2 and 4");
			printMessage("Enter the number of players:");
			while (!scanner.hasNextInt()) {
				printMessage(scanner.next() + " is not numerical value!");
				printMessage("Enter the number of players:");
			}
			number = scanner.nextInt();
		}
		return number;
	}

	private void showStartGameView() {
		printMessage("Let's start the game!");
	}

	private void showBiddingView() {
		printMessage("Players bid to see who will be first. The player who bids the most, must pay with the coins. Other players do not pay coins. If the bids are tied for most, starts the player which start to bid first.");
	}

	private void showCardsView() {
		Card[] cards = game.getAllowedCards();
		printMessage("It's " + game.getCurrentPlayer().getName()
				+ " turn. You have " + game.getCurrentPlayer().getCoins()
				+ " coins. You can choose from cards:");
		for (int i = 0; i < cards.length; i++) {
			printMessage((i + 1) + ".");
			printMessage("Resource: "
					+ cards[i].getResource().getName().toString()
					+ (cards[i].getResourceNumber() == 2 ? " (double resource)"
							: ""));
			printMessage("Points of victory for cards with this resource:");
			printMessage("1 point if you have "
					+ cards[i].getResource().getPoints().get(1) + " cards;");
			printMessage("2 points if you have "
					+ cards[i].getResource().getPoints().get(2) + " cards;");
			printMessage("3 points if you have "
					+ cards[i].getResource().getPoints().get(3) + " cards;");
			printMessage("5 points if you have "
					+ cards[i].getResource().getPoints().get(5) + " cards;");
			if (CardType.SINGLE.equals(cards[i].getType())) {
				printMessage("Action: " + cards[i].getFirstActionInString()
						+ " (with " + cards[i].getActionPoints() + " credits)");
			} else {
				printMessage("Card with two actions"
						+ (CardType.DOUBLE.equals(cards[i].getType()) ? ":"
								: " to choose:"));
				printMessage("First action: "
						+ cards[i].getFirstActionInString() + " (with "
						+ cards[i].getActionPoints() + " credits)");
				printMessage("Second action: "
						+ cards[i].getSecondActionInString() + " (with "
						+ cards[i].getSecondActionPoints() + " credits)");
			}
		}
	}

	private void showActionsView() {
		Card card = game.getCurrentCard();
		printMessage("This card have two actions:");
		printMessage("1. Action " + card.getFirstActionInString() + " with "
				+ card.getActionPoints() + " credits.");
		printMessage("2. Action " + card.getSecondActionInString() + " with "
				+ card.getSecondActionPoints() + " credits.");
	}

	private void showArmiesView() {
		List<Army> armies = game.getAllowedArmies();
		printMessage("Allowed armies to make an action:");
		for (int i = 0; i < armies.size(); i++) {
			printMessage((i + 1) + ". Army of player "
					+ armies.get(i).getOwner().getName() + " on territory nr "
					+ armies.get(i).getPlace().getId() + " (in continent nr "
					+ armies.get(i).getPlace().getContinent() + ")");
		}
	}

	private void showResultsView() {
		int i = 1;
		for (Map.Entry<Player, Integer> result : game.getResults().entrySet()) {
			printMessage((i++) + ". " + result.getKey() + " collected "
					+ result.getValue() + " pts");
		}
	}

	private void showTerritoriesView() {
		List<Territory> territories = game.getAllowedTerritories();
		printMessage("Allowed territories to make an action:");
		for (int i = 0; i < territories.size(); i++) {
			printMessage((i + 1) + ". Territory nr "
					+ territories.get(i).getId() + " (in continent nr "
					+ territories.get(i).getContinent() + ")");
		}
	}

	private void showActionInformation() {
		Card card = game.getCurrentCard();
		String action = null;
		String explenation = null;
		int points = game.getCredits();
		action = card.getFirstActionInString();
		if (ActionName.DESTROY_ARMY.equals(card.getActionName())) {
			explenation = "You can remove armies belonging to any player from board. You have still "
					+ points + " credits to use.";
		} else if (ActionName.MOVE_ARMY.equals(card.getActionName())) {
			explenation = "You can move your army to adiacent territory only by land. You have still "
					+ points + " move credits to use.";
		} else if (ActionName.MOVE_ARMY_BY_SEA.equals(card.getActionName())) {
			explenation = "You can move your army to adiacent territory by land and sea. You have still "
					+ points + " move credits to use.";
		} else if (ActionName.PLACE_ARMY.equals(card.getActionName())) {
			explenation = "You can place armies only on starting region or on an area where you have a city. You have still "
					+ points + " place credits to use.";
		} else if (ActionName.PLACE_CITY.equals(card.getActionName())) {
			explenation = "You can build a city anywhere on board you have an army. You have still "
					+ points + " build credits to use.";
		}
		printMessage("Your action is " + action + ".");
		printMessage(explenation);

	}

	private void printMessage(String message) {
		System.out.println(message);
	}

	@Override
	public void update() {
		update(null, null);
	}

}
