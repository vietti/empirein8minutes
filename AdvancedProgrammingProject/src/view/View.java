package view;

import java.util.Map;
import java.util.Observer;

import model.dto.ActionName;
import model.dto.Army;
import model.dto.Territory;

public interface View extends Observer {
	void update();
}
