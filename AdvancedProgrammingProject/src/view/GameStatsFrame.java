package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import model.dto.Player;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Map;

public class GameStatsFrame extends JFrame {

	private JPanel contentPane;
	JLabel[] lbls;

	/**
	 * Create the frame.
	 */
	public GameStatsFrame(Map<Player, Integer> stats) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{438, 0};
		gbl_contentPane.rowHeights = new int[]{17, 44, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblTheGameIs = new JLabel("The game is end. Congratulations for the winner!");
		lblTheGameIs.setFont(new Font("Dialog", Font.BOLD, 14));
		lblTheGameIs.setForeground(Color.GRAY);
		GridBagConstraints gbc_lblTheGameIs = new GridBagConstraints();
		gbc_lblTheGameIs.fill = GridBagConstraints.BOTH;
		gbc_lblTheGameIs.insets = new Insets(0, 0, 5, 0);
		gbc_lblTheGameIs.gridx = 0;
		gbc_lblTheGameIs.gridy = 0;
		contentPane.add(lblTheGameIs, gbc_lblTheGameIs);
		lbls = new JLabel[stats.size()];
		int i = 0;
		for(Map.Entry<Player, Integer> result : stats.entrySet()){
			lbls[i] = new JLabel((i+1) + ". " + result.getKey().getName() + " - " + result.getValue() + " points");
			lbls[i].setForeground(result.getKey().getColor());
			GridBagConstraints gbc_lblst = new GridBagConstraints();
			gbc_lblst.fill = GridBagConstraints.BOTH;
			gbc_lblst.gridx = 0;
			gbc_lblst.gridy = i + 1;
			contentPane.add(lbls[i], gbc_lblst);
			i++;
		}
	}

}
