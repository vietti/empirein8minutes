package view;

import javax.swing.JPanel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.SpringLayout;
import javax.swing.JLabel;

import model.Game;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class StartPanel extends JPanel {

	Game game;
	/**
	 * Create the panel.
	 * @param guiView 
	 */
	public StartPanel(final Game game) {
		
		this.game = game;
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{81, 308, 0};
		gridBagLayout.rowHeights = new int[]{35, 19, 29, 25, 25, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JButton btnTwoPlayers = new JButton("Two players");
		btnTwoPlayers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				game.defineNumbersOfPlayers(2);
			}
		});
		
		JLabel lblStartNewGame = new JLabel("Start new game. Choose number of players");
		GridBagConstraints gbc_lblStartNewGame = new GridBagConstraints();
		gbc_lblStartNewGame.anchor = GridBagConstraints.WEST;
		gbc_lblStartNewGame.fill = GridBagConstraints.VERTICAL;
		gbc_lblStartNewGame.insets = new Insets(0, 0, 5, 0);
		gbc_lblStartNewGame.gridx = 1;
		gbc_lblStartNewGame.gridy = 1;
		add(lblStartNewGame, gbc_lblStartNewGame);
		GridBagConstraints gbc_btnTwoPlayers = new GridBagConstraints();
		gbc_btnTwoPlayers.fill = GridBagConstraints.BOTH;
		gbc_btnTwoPlayers.insets = new Insets(0, 0, 5, 0);
		gbc_btnTwoPlayers.gridx = 1;
		gbc_btnTwoPlayers.gridy = 2;
		add(btnTwoPlayers, gbc_btnTwoPlayers);
		
		JButton btnFourPlayers = new JButton("Four players");
		btnFourPlayers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				game.defineNumbersOfPlayers(4);
			}
		});
		
		JButton btnThreePlayers = new JButton("Three players");
		btnThreePlayers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				game.defineNumbersOfPlayers(3);
			}
		});
		GridBagConstraints gbc_btnThreePlayers = new GridBagConstraints();
		gbc_btnThreePlayers.anchor = GridBagConstraints.NORTH;
		gbc_btnThreePlayers.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnThreePlayers.insets = new Insets(0, 0, 5, 0);
		gbc_btnThreePlayers.gridx = 1;
		gbc_btnThreePlayers.gridy = 3;
		add(btnThreePlayers, gbc_btnThreePlayers);
		GridBagConstraints gbc_btnFourPlayers = new GridBagConstraints();
		gbc_btnFourPlayers.anchor = GridBagConstraints.NORTH;
		gbc_btnFourPlayers.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnFourPlayers.gridx = 1;
		gbc_btnFourPlayers.gridy = 4;
		add(btnFourPlayers, gbc_btnFourPlayers);

	}
}
