package view;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.JTextField;

import java.awt.Insets;

import javax.swing.JButton;

import model.Game;
import model.dto.Card;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BidPanel extends JPanel {
	private JTextField txtTypeNameHere;
	private JTextField txtTypeBidHere;
	private JButton btnNext;

	Game game;

	/**
	 * Create the panel.
	 */
	public BidPanel(final Game game) {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 148, 153, 0 };
		gridBagLayout.rowHeights = new int[] { 15, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		setLayout(gridBagLayout);

		this.game = game;

		JLabel lblEnterNameOf = new JLabel("Enter name of player:");
		GridBagConstraints gbc_lblEnterNameOf = new GridBagConstraints();
		gbc_lblEnterNameOf.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnterNameOf.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblEnterNameOf.gridx = 0;
		gbc_lblEnterNameOf.gridy = 1;
		add(lblEnterNameOf, gbc_lblEnterNameOf);

		txtTypeNameHere = new JTextField();
		GridBagConstraints gbc_txtTypeNameHere = new GridBagConstraints();
		gbc_txtTypeNameHere.insets = new Insets(0, 0, 5, 0);
		gbc_txtTypeNameHere.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtTypeNameHere.gridx = 1;
		gbc_txtTypeNameHere.gridy = 1;
		add(txtTypeNameHere, gbc_txtTypeNameHere);
		txtTypeNameHere.setColumns(10);

		JLabel lblEnterBidFrom = new JLabel("Enter bid from this player:");
		GridBagConstraints gbc_lblEnterBidFrom = new GridBagConstraints();
		gbc_lblEnterBidFrom.anchor = GridBagConstraints.EAST;
		gbc_lblEnterBidFrom.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnterBidFrom.gridx = 0;
		gbc_lblEnterBidFrom.gridy = 3;
		add(lblEnterBidFrom, gbc_lblEnterBidFrom);

		txtTypeBidHere = new JTextField();
		GridBagConstraints gbc_txtTypeBidHere = new GridBagConstraints();
		gbc_txtTypeBidHere.insets = new Insets(0, 0, 5, 0);
		gbc_txtTypeBidHere.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtTypeBidHere.gridx = 1;
		gbc_txtTypeBidHere.gridy = 3;
		add(txtTypeBidHere, gbc_txtTypeBidHere);
		txtTypeBidHere.setColumns(10);

		btnNext = new JButton("Next");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = txtTypeNameHere.getText();
				if(name.isEmpty()) {
					JOptionPane.showMessageDialog(null,
							"Enter your name", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
				int bid;
				try {
					bid = Integer.parseInt(txtTypeBidHere.getText());
				} catch (NumberFormatException exception) {
					JOptionPane.showMessageDialog(null,
							"Bid should be number", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				int maxBid;
				if (game.getDefinedPlayersCount() == 2) {
					maxBid = 8;
				} else if (game.getDefinedPlayersCount() == 3) {
					maxBid = 10;
				} else {
					maxBid = 12;
				}

				if (bid < 0 || bid > maxBid) {
					JOptionPane.showMessageDialog(null,
							"Bid should be number between 0 and " + maxBid,
							"Error", JOptionPane.ERROR_MESSAGE);
				} else {
					game.bid(name, bid);
				}
			}
		});
		GridBagConstraints gbc_btnNext = new GridBagConstraints();
		gbc_btnNext.gridx = 1;
		gbc_btnNext.gridy = 5;
		add(btnNext, gbc_btnNext);

	}
	
	public void update() {
		txtTypeBidHere.setText("");
		txtTypeNameHere.setText("");
	}

}
