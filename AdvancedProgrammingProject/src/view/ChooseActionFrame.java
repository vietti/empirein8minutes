package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

import model.Game;
import model.dto.ActionName;

public class ChooseActionFrame extends JFrame {

	private JPanel contentPane;

	Game game;
	/**
	 * Create the frame.
	 */
	public ChooseActionFrame(Game game) {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblChooseActionTo = new JLabel("Choose action to use:");
		lblChooseActionTo.setBounds(140, 67, 186, 15);
		contentPane.add(lblChooseActionTo);

		JButton btnFirstaction = new JButton("<html>" + game.getCurrentCard().getFirstActionInString() + " - " + game.getCurrentCard().getActionPoints() + " time(s)</html>" );
		btnFirstaction.setBounds(12, 94, 155, 66);
		btnFirstaction.addActionListener(new ChooseActionListener(game.getCurrentCard().getActionName(), game, this));
		contentPane.add(btnFirstaction);
		
		JButton btnSecondaction = new JButton("<html>" + game.getCurrentCard().getSecondActionInString() + " - " + game.getCurrentCard().getSecondActionPoints() + " time(s)</html>");
		btnSecondaction.setBounds(281, 94, 155, 66);
		btnSecondaction.addActionListener(new ChooseActionListener(game.getCurrentCard().getSecondActionName(), game, this));
		contentPane.add(btnSecondaction);
	}

	class ChooseActionListener implements ActionListener {
		private final ActionName action;
		private final Game game;
		private final ChooseActionFrame frame;

		ChooseActionListener(final ActionName action, final Game game, ChooseActionFrame frame) {
			super();
			this.action = action;
			this.game = game;
			this.frame = frame;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			game.chooseAction(action);
			frame.dispose();
		}
	}
}
