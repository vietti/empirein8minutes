package model.state;

import model.Game;
import model.dto.ActionName;
import model.dto.Army;
import model.dto.City;
import model.dto.Territory;

public class TerritoryChosenState implements State {
	Game game;

	public TerritoryChosenState(Game game) {
		this.game = game;
	}

	@Override
	public void defineNumbersOfPlayers(int number) {
		// TODO Auto-generated method stub

	}

	@Override
	public void bid(String playerName, int amount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseCard(int position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseAction(ActionName action) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseArmy(Army army) {
		ActionName action = game.getCurrentCard().getActionName();
		switch (action) {
		case MOVE_ARMY: {
			game.setChosenArmy(army);
			// TODO
			game.unsetAllowedArmies();
			game.filterAllowedTerritories(action);
			break;
		}
		case MOVE_ARMY_BY_SEA: {
			game.setChosenArmy(army);
			game.unsetAllowedArmies();
			// TODO
			game.filterAllowedTerritories(action);
			break;
		}
		default:
			break;
		}
		if (game.checkIfCreditsUsed()) {
			game.unsetAllowedArmies();
			game.unsetAllowedTerritories();
			game.nextTurn();
		} else
			game.setState(game.getArmyChosenState());
	}

	@Override
	public void chooseTerritory(Territory territory) {
		ActionName action = game.getCurrentCard().getActionName();
		switch (action) {
		case PLACE_ARMY: {
			Army newArmy = new Army(game.getCurrentPlayer(), territory);
			game.addArmy(newArmy);
			game.subtractCredits(1);
			game.filterAllowedTerritories(action);
			break;
		}
		case PLACE_CITY: {
			City newCity = new City(game.getCurrentPlayer(), territory);
			game.addCity(newCity);
			game.subtractCredits(1);
			game.filterAllowedTerritories(action);
			break;
		}
		default:
			break;
		}
		if (game.checkIfCreditsUsed()) {
			game.unsetAllowedArmies();
			game.unsetAllowedTerritories();
			game.nextTurn();
		} else
			game.setState(game.getTerritoryChosenState());
	}

	@Override
	public void resignFromAction() {
		game.unsetAllowedArmies();
		game.unsetAllowedTerritories();
		game.nextTurn();

	}

	@Override
	public String toString() {
		return this.getClass().getName();
	}

}
