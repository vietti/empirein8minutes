package model.state;

import model.Game;
import model.dto.ActionName;
import model.dto.Army;
import model.dto.Player;
import model.dto.Territory;

public class BiddingState implements State {
	Game game;

	public BiddingState(Game game) {
		this.game = game;
	}

	@Override
	public void defineNumbersOfPlayers(int number) {
		// TODO Auto-generated method stub

	}

	@Override
	public void bid(String playerName, int amount) {
		Player player = new Player(playerName, amount, game.getPlayersNumber(),
				game.getPlayers().size());
		game.addPlayer(player);
		game.addArmy(new Army(player, game.getTerritory(0)));
		game.addArmy(new Army(player, game.getTerritory(0)));
		game.addArmy(new Army(player, game.getTerritory(0)));
		if (game.getPlayers().size() == game.getPlayersNumber()) {
			game.setEveryoneBid();// potem do widoku
			game.orderPlayers();// no to sie jeszcze poprawi
			game.nextTurn();

			game.setState(game.getBeforeChoosingCardState());
		}
	}

	@Override
	public void chooseCard(int position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseAction(ActionName action) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseArmy(Army army) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseTerritory(Territory territory) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resignFromAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return this.getClass().getName();
	}
}
