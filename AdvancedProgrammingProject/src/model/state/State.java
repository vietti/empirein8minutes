package model.state;

import model.dto.ActionName;
import model.dto.Army;
import model.dto.Territory;

public interface State {
	void defineNumbersOfPlayers(int number);

	void bid(String playerName, int amount);

	void chooseCard(int position);

	void chooseAction(ActionName action);

	void chooseArmy(Army army);

	void chooseTerritory(Territory territory);

	void resignFromAction();
}
