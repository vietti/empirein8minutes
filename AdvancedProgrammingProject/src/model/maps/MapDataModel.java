package model.maps;

import model.maps.IMapData;
import model.maps.MapDataModel;
import model.maps.MapData;

import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URL;

public class MapDataModel extends RegionModel implements IMapData {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int modelCount = 0;

	public MapDataModel(IMapData m) {
		super(m);
		modelCount++;
		System.out.println(modelCount);
	}

	public MapDataModel(String mapPath) {
		super(null);
		modelCount++;
		URL url = Resources.getResourceFile(mapPath);
		loadMap(url, this);
	}

	@Override
	public void setMapBackground(File f) {
		((IMapData) r).setMapBackground(f);
		setChanged();
		notifyObservers();
	}

	public void setMapData(IMapData md) {
		System.out.println("Changing model from " + this.r + " to " + md);
		this.r = md;
		setChanged();
		notifyObservers();
	}

	public IMapData getMapData() {
		return (IMapData) r;
	}

	@Override
	public Image getMapBackground() {
		return ((IMapData) r).getMapBackground();
	}

	private static void loadMap(URL url, MapDataModel model) {
		try {
			ObjectInputStream ois = new ObjectInputStream(url.openStream());
			IMapData mr = (IMapData) (ois.readObject());
			model.setMapData(mr);
			ois.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}