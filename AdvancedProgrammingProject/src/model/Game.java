package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import model.dto.ActionName;
import model.dto.Army;
import model.dto.Card;
import model.dto.City;
import model.dto.Player;
import model.dto.TerritoriesConnection;
import model.dto.Territory;
import model.state.ArmyChosenState;
import model.state.BeforeChoosingCardState;
import model.state.BiddingState;
import model.state.CardChosenState;
import model.state.StartGameState;
import model.state.State;
import model.state.TerritoryChosenState;
import utils.MapUtils;

public class Game extends Model {
	// TODO RESIGN
	State startGameState;
	State biddingState;
	State beforeChoosingCardState;
	State cardChosenState;
	State armyChosenState;
	State territoryChosenState;

	State state;
	int playersNumber = 0;
	List<Player> players;
	ListIterator<Player> playersIterator;
	private Player currentPlayer;
	private Player winner;
	private Card currentCard;
	private Table table;

	private List<Territory> allowedTerritories;
	private List<Army> allowedArmies;
	private Card[] allowedCards;

	private List<Army> armies;
	private List<City> cities;
	private Army chosenArmy;
	private TerritoriesConnection territoriesConnection;
	private Map<Territory, Player> territoryControllers;
	private Map<Player, Integer> results;

	private boolean chosenAction;
	private boolean secondAction;
	private boolean twoActions;
	private boolean everyoneBid;
	private int credits;

	public Game() {
		startGameState = new StartGameState(this);
		biddingState = new BiddingState(this);
		beforeChoosingCardState = new BeforeChoosingCardState(this);
		cardChosenState = new CardChosenState(this);
		armyChosenState = new ArmyChosenState(this);
		territoryChosenState = new TerritoryChosenState(this);

		players = new LinkedList<Player>();

		armies = new ArrayList<Army>();
		cities = new ArrayList<City>();

		allowedCards = new Card[6];
		allowedArmies = new ArrayList<Army>();
		allowedTerritories = new ArrayList<Territory>();

		twoActions = false;
		secondAction = false;

		state = startGameState;

		territoriesConnection = new TerritoriesConnection();
		territoryControllers = new HashMap<Territory, Player>();

		setChanged();
		notifyViews();
	}

	public void defineNumbersOfPlayers(int number) {
		state.defineNumbersOfPlayers(number);
		setChanged();
		notifyViews();
	};

	public void bid(String playerName, int amount) {
		state.bid(playerName, amount);
		setChanged();
		notifyViews();
	};

	public void chooseCard(int position) {
		state.chooseCard(position);
		setChanged();
		notifyViews();
	};

	public void chooseAction(ActionName action) {
		state.chooseAction(action);
		setChanged();
		notifyViews();
	};

	public void chooseArmy(Army army) {
		state.chooseArmy(army);
		setChanged();
		notifyViews();
	};

	public void chooseTerritory(Territory territory) {
		state.chooseTerritory(territory);
		setChanged();
		notifyViews();
	};

	public void resignFromAction() {
		state.resignFromAction();
		setChanged();
		notifyViews();
	};

	public void createTable() {
		table = new Table();
	}

	public Card getCardAtPosition(int position) {
		return table.getCardAtPosition(position);
	}

	public int getPriceAtPosition(int position) {
		return table.getPriceAtPosition(position);
	}

	public void fillTable() {
		table.fillTable();
	}

	public Card[] getCards() {
		return table.getCards();
	}

	public Card getCurrentCard() {
		return currentCard;
	}

	public void setCurrentCard(Card currentCard) {
		this.currentCard = currentCard;
	}

	public void addPlayer(Player player) {
		players.add(player);
	}

	public List<Player> getPlayers() {
		return players;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(Player player) {
		this.currentPlayer = player;
	}

	public Player getNextPlayer() {
		if (!playersIterator.hasNext())
			playersIterator = players.listIterator(0);
		return playersIterator.next();
	}

	public void orderPlayers() {
		List<Player> orderedByBids = new ArrayList<>(players);
		Collections.sort(orderedByBids, new Comparator<Player>() {
			@Override
			public int compare(Player p1, Player p2) {
				return Integer.valueOf(p2.getBid()).compareTo(p1.getBid());
			}
		});
		if (orderedByBids.get(0).getBid() != orderedByBids.get(1).getBid()) {
			int indexOfWinner = players.indexOf(orderedByBids.get(0));
			Player player = players.get(indexOfWinner);
			players.remove(indexOfWinner);
			players.add(0, player);
			player.subtractCoins(player.getBid());
		}
		System.out.println(players);
		playersIterator = players.listIterator();
	}

	public int getDefinedPlayersCount() {
		return players.size();
	}

	public void setPlayersNumber(int playersNumber) {
		this.playersNumber = playersNumber;
	}

	public int getPlayersNumber() {
		return playersNumber;
	}

	public void setState(State state) {
		System.out.println(this.state + " => " + state);
		this.state = state;
	}

	public State getState() {
		return state;
	}

	public State getArmyChosenState() {
		return armyChosenState;
	}

	public State getBeforeChoosingCardState() {
		return beforeChoosingCardState;
	}

	public State getBiddingState() {
		return biddingState;
	}

	public State getCardChosenState() {
		return cardChosenState;
	}

	public State getStartGameState() {
		return startGameState;
	}

	public State getTerritoryChosenState() {
		return territoryChosenState;
	}

	public boolean isEveryoneBid() {
		return everyoneBid;
	}

	public void setEveryoneBid() {
		this.everyoneBid = true;
	}

	public int getCredits() {
		return credits;
	}

	public void setCredits(int credits) {
		this.credits = credits;
	}

	public void subtractCredits(int credits) {
		this.credits -= credits;
	}

	public List<Territory> getAllowedTerritories() {
		return allowedTerritories;
	}

	public void setAllowedTerritories(List<Territory> allowedTerritories) {
		this.allowedTerritories = allowedTerritories;
	}

	public void unsetAllowedTerritories() {
		this.allowedTerritories = new ArrayList<Territory>();
	}

	public List<Army> getAllowedArmies() {
		return allowedArmies;
	}

	public List<Territory> getAllowedArmiesTerritories() {
		List<Territory> allowedArmiesTerritories = new ArrayList<Territory>();
		for (Army a : allowedArmies) {
			if (!allowedArmiesTerritories.contains(a.getPlace())) {
				allowedArmiesTerritories.add(a.getPlace());
			}
		}
		return allowedArmiesTerritories;
	}

	public List<Army> getAllowedArmiesForTerritory(Territory t) {
		List<Army> allowedArmiesForTerritory = new ArrayList<Army>();
		for (Army a : allowedArmies) {
			if (a.getPlace().equals(t)) {
				allowedArmiesForTerritory.add(a);
			}
		}
		return allowedArmiesForTerritory;
	}

	public void setAllowedArmies(List<Army> allowedArmies) {
		this.allowedArmies = allowedArmies;
	}

	public void unsetAllowedArmies() {
		this.allowedArmies = new ArrayList<Army>();
	}

	public Card[] getAllowedCards() {
		return allowedCards;
	}

	public void setAllowedCards(Card[] allowedCards) {
		this.allowedCards = allowedCards;
	}

	public boolean isTwoActions() {
		return twoActions;
	}

	public void setTwoActions(boolean twoActions) {
		this.twoActions = twoActions;
	}

	public boolean isSecondAction() {
		return secondAction;
	}

	public void setSecondAction(boolean secondAction) {
		this.secondAction = secondAction;
	}

	public void filterAllowedTerritories(ActionName action) {
		allowedTerritories = new ArrayList<Territory>();
		switch (action) {
		case PLACE_CITY: {
			for (Army army : armies) {
				if (currentPlayer.equals(army.getOwner())
						&& !allowedTerritories.contains(army.getPlace())) {
					allowedTerritories.add(army.getPlace());
				}
			}
			break;
		}
		case PLACE_ARMY: {
			allowedTerritories.add(territoriesConnection.getTerritory(0));
			for (City c : cities) {
				if (currentPlayer.equals(c.getOwner())
						&& !allowedTerritories.contains(c.getPlace())) {
					allowedTerritories.add(c.getPlace());
				}
			}
			break;
		}
		case MOVE_ARMY: {
			if (chosenArmy != null) {
				allowedTerritories = territoriesConnection
						.getNeighborsByLand(chosenArmy.getPlace());
				;
			}
			break;
		}
		case MOVE_ARMY_BY_SEA: {
			if (chosenArmy != null) {
				allowedTerritories = territoriesConnection
						.getNeighborsBySea(chosenArmy.getPlace());
			}
			break;
		}

		default:
			break;
		}
	}

	public void filterAllowedArmies(ActionName action) {
		allowedArmies = new ArrayList<Army>();
		switch (action) {
		case MOVE_ARMY:
		case MOVE_ARMY_BY_SEA: {
			for (Army army : armies) {
				if (currentPlayer.equals(army.getOwner())) {
					allowedArmies.add(army);
				}
			}
			break;
		}
		case DESTROY_ARMY: {
			for (Army army : armies) {
				if (!currentPlayer.equals(army.getOwner())) {
					allowedArmies.add(army);
				}
			}
			break;
		}
		default:
			break;
		}
	}

	public void filterAllowedCards() {
		for (int i = 0; i < table.getCards().length; i++) {
			if (currentPlayer.getCoins() >= table.getPriceAtPosition(i)) {
				System.out.println(table.showCardAtPosition(i));
				allowedCards[i] = table.showCardAtPosition(i);
			} else {
				allowedCards[i] = null;
			}
		}
	}

	public void addArmy(Army army) {
		armies.add(army);
	}

	public List<Army> getArmies() {
		return armies;
	}

	public List<Army> getArmiesForTerritory(Territory t) {
		List<Army> armiesForTerritory = new ArrayList<Army>();
		for (Army a : armies) {
			if (a.getPlace().equals(t)) {
				armiesForTerritory.add(a);
			}
		}
		return armiesForTerritory;
	}

	public List<Army> getRivalsArmiesForTerritory(Territory t) {
		List<Army> rivalsArmiesForTerritory = new ArrayList<Army>();
		for (Army a : armies) {
			if (a.getPlace().equals(t) && !a.getOwner().equals(currentPlayer)) {
				rivalsArmiesForTerritory.add(a);
			}
		}
		return rivalsArmiesForTerritory;
	}

	public List<City> getCitiesForTerritory(Territory t) {
		List<City> citiesForTerritory = new ArrayList<City>();
		for (City c : cities) {
			if (c.getPlace().equals(t)) {
				citiesForTerritory.add(c);
			}
		}
		return citiesForTerritory;
	}

	public Army getArmy(Army army) {
		return armies.get(armies.indexOf(army));
	}

	public void removeArmy(Army army) {
		armies.remove(army);
	}

	public void addCity(City city) {
		cities.add(city);
	}

	public void nextTurn() {
		// if one action or two and it was second
		if (!twoActions || (twoActions && secondAction)) {
			if (isGameFinished()) {
				setState(getStartGameState());
				countUpPointsAndRevealWinner();
				setChanged();
				notifyViews();
			} else {
				setState(getBeforeChoosingCardState());
				fillTable();
				currentPlayer = getNextPlayer();
				filterAllowedCards();
				chosenAction = false;
			}
			// if two and it was first
		} else {
			currentCard.setSecondActionAsFirst();
			setCredits(currentCard.getActionPoints());
			filterAllowedArmies(currentCard.getActionName());
			filterAllowedTerritories(currentCard.getActionName());
			secondAction = true;
			setChanged();
			notifyViews();
		}
	}

	public void countUpPointsAndRevealWinner() {
		Map<Player, Integer> points = new HashMap<Player, Integer>();
		for (Player p : players) {
			points.put(p, 0);
		}
		for (Territory t : territoriesConnection.getTerritories()) {
			Player p = t.getTerritoryController(this);
			if (p != null) {
				points.put(p, points.get(p) + 1);
				territoryControllers.put(t, p);
			}
		}
		for (int i = 1; i <= 5; i++) {
			Player p = Territory.getContinentController(i, this);
			if (p != null) {
				points.put(p, points.get(p) + 1);
			}
		}
		for (Player p : players) {
			points.put(p, points.get(p) + p.getResourcePoints());
		}
		results = MapUtils.sortByValue(points);
	}

	public boolean isGameFinished() {
		if ((state instanceof StartGameState && playersNumber == 0)
				|| state instanceof BiddingState)
			return false;
		for (Player p : players) {
			if (!p.hasEndedGame(playersNumber)) {
				return false;
			}
		}
		return true;
	}

	public boolean checkIfCreditsUsed() {
		return credits <= 0;
	}

	public Army getChosenArmy() {
		return chosenArmy;
	}

	public void setChosenArmy(Army chosenArmy) {
		this.chosenArmy = chosenArmy;
	}

	public List<Territory> getNeighborsByLand(Territory territory) {
		return territoriesConnection.getNeighborsByLand(territory);
	}

	public List<Territory> getNeighborsBySea(Territory territory) {
		return territoriesConnection.getNeighborsBySea(territory);
	}

	public Territory getTerritory(int number) {
		return territoriesConnection.getTerritory(number);
	}

	public Map<Territory, Player> getTerritoryControllers() {
		return territoryControllers;
	}

	public List<City> getCities() {
		return cities;
	}

	public Player getWinner() {
		return winner;
	}

	public boolean isChosenAction() {
		return chosenAction;
	}

	public void setChosenAction(boolean isChosenAction) {
		this.chosenAction = isChosenAction;
	}

	public Map<Player, Integer> getResults() {
		return results;
	}
}
