package model;

import model.dto.Card;
import utils.CardChooser;

public class Table {
	private CardChooser cardChooser;
	private Card[] table;
	private int[] prices = { 0, 1, 1, 2, 2, 3 };
	private boolean empty;

	public Table() {
		cardChooser = new CardChooser();
		table = new Card[6];
		empty = true;
	}

	public String printTable() {
		String cards = "";
		for (int i = 0; i < table.length; i++) {
			cards += prices[i];
			cards += " => ";
			cards += table[i] == null ? "Empty\n" : table[i] + "\n";
		}
		return cards;
	}

	public Card[] getCards() {
		return table;
	}

	public void fillTable() {
		if (empty) {
			System.out.println("6 cards drawn");// TODO delete
			drawCards();
			empty = false;
		} else {
			System.out.println("One card drawn");// TODO delete
			drawOneCard();
		}
	}

	private void drawCards() {
		for (int i = 0; i < table.length; i++) {
			table[i] = cardChooser.getCard();
		}
	}

	private void drawOneCard() {
		for (int i = 0; i < table.length; i++) {
			// pusta pozycja
			if (table[i] == null) {
				// jesli to ostatnia to wyciagamy
				if (i == (table.length - 1))
					table[i] = cardChooser.getCard();
				// inaczej przesuwamy karte z prawej
				else
					slideCardFromRightSide(i);
			}
		}
	}

	private void slideCardFromRightSide(int i) {
		Card temp = table[i];
		table[i] = table[i + 1];
		table[i + 1] = temp;
	}

	public Card getCardAtPosition(int position) {
		Card chosenCard = table[position];
		table[position] = null;
		return chosenCard;
	}

	public Card showCardAtPosition(int position) {
		return table[position];
	}

	public int getPriceAtPosition(int position) {
		return prices[position];
	}

}
