package model.dto;

public class Card {
	private Resource resource;
	private int resourceNumber;
	private CardType type;
	private ActionName actionName;
	private int actionPoints;
	private ActionName secondActionName;
	private int secondActionPoints;

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public int getResourceNumber() {
		return resourceNumber;
	}

	public void setResourceNumber(int resourceNumber) {
		this.resourceNumber = resourceNumber;
	}

	public CardType getType() {
		return type;
	}

	public void setType(CardType type) {
		this.type = type;
	}

	public Card(ResourceName resource, int resourceNumber, ActionName name,
			Integer credits) {
		this.resource = new Resource(resource);
		this.resourceNumber = resourceNumber;
		this.actionName = name;
		this.actionPoints = credits;
		this.type = CardType.SINGLE;
	}

	public Card(ResourceName resource, int resourceNumber, CardType type,
			ActionName firstName, Integer firstCredits, ActionName secondName,
			Integer secondCredits) {
		this.resource = new Resource(resource);
		this.resourceNumber = resourceNumber;
		this.type = type;
		this.actionName = firstName;
		this.actionPoints = firstCredits;
		this.secondActionName = secondName;
		this.secondActionPoints = secondCredits;
	}

	public ActionName getActionName() {
		return actionName;
	}

	public void setActionName(ActionName firstActionName) {
		this.actionName = firstActionName;
	}

	public int getActionPoints() {
		return actionPoints;
	}

	public void setActionPoints(int firstActionPoints) {
		this.actionPoints = firstActionPoints;
	}

	public ActionName getSecondActionName() {
		return secondActionName;
	}

	public void setSecondActionName(ActionName secondActionName) {
		this.secondActionName = secondActionName;
	}

	public int getSecondActionPoints() {
		return secondActionPoints;
	}

	public void setSecondActionPoints(int secondActionPoints) {
		this.secondActionPoints = secondActionPoints;
	}

	@Override
	public String toString() {
		return "Card [resource=" + resource + ", resourceNumber="
				+ resourceNumber + ", type=" + type + ", actionName="
				+ actionName + ", actionPoints=" + actionPoints
				+ ", secondActionName=" + secondActionName
				+ ", secondActionPoints=" + secondActionPoints + "]";
	}

	public void setActionAsFirst(ActionName name) {
		if (name.equals(actionName)) {
			return;
		} else {
			secondActionName = actionName;
			actionName = name;
			int p = secondActionPoints;
			secondActionPoints = actionPoints;
			actionPoints = p;
		}
	}

	public void setSecondActionAsFirst() {
		actionName = secondActionName;
		secondActionName = null;
		actionPoints = secondActionPoints;
		secondActionPoints = 0;
	}

	public String getFirstActionInString() {
		if (ActionName.DESTROY_ARMY.equals(actionName)) {
			return "destroy armies";
		} else if (ActionName.MOVE_ARMY.equals(actionName)) {
			return "move armies by land";
		} else if (ActionName.MOVE_ARMY_BY_SEA.equals(actionName)) {
			return "move armies by sea and land";
		} else if (ActionName.PLACE_ARMY.equals(actionName)) {
			return "place new armies";
		} else if (ActionName.PLACE_CITY.equals(actionName)) {
			return "build a city";
		}
		return null;
	}

	public String getSecondActionInString() {
		if (ActionName.DESTROY_ARMY.equals(secondActionName)) {
			return "destroy armies";
		} else if (ActionName.MOVE_ARMY.equals(secondActionName)) {
			return "movie armies by land";
		} else if (ActionName.MOVE_ARMY_BY_SEA.equals(secondActionName)) {
			return "movie armies by sea and land";
		} else if (ActionName.PLACE_ARMY.equals(secondActionName)) {
			return "place new armies";
		} else if (ActionName.PLACE_CITY.equals(secondActionName)) {
			return "build a city";
		}
		return null;
	}

	// public String getResourceNameInString() {
	// if(ResourceName.ALL.equals(resource.getName())) {
	// return "all resource";
	// } else if(ResourceName.CARROT.equals(resource.getName())) {
	// return "carrot";
	// }
	// }
}