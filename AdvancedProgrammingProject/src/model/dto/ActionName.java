package model.dto;

public enum ActionName {
	PLACE_CITY, PLACE_ARMY, MOVE_ARMY, MOVE_ARMY_BY_SEA, DESTROY_ARMY
}
