package model.dto;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Player {
	private String name;
	private int bid;
	private int coins;
	private int armies;
	private int cities;
	private Color color;
	private List<Card> cards;

	public Player(String name, int bid, int numberOfPlayers, int playersCount) {
		this.name = name;
		this.bid = bid;
		this.armies = 11;
		this.cities = 3;
		if (numberOfPlayers == 2) {
			this.coins = 12;
		} else if (numberOfPlayers == 3) {
			this.coins = 10;
		} else if (numberOfPlayers == 4) {
			this.coins = 8;
		} else
			this.coins = 0;
		cards = new ArrayList<Card>();
		switch (playersCount) {
		case 0: {
			color = Color.BLUE;
			break;
		}
		case 1: {
			color = Color.RED;
			break;
		}
		case 2: {
			color = Color.GREEN;
			break;
		}
		case 3: {
			color = Color.YELLOW;
			break;
		}
		default:
			color = Color.BLACK;
			break;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBid() {
		return bid;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}

	public int getCoins() {
		return coins;
	}

	public void setCoins(int coins) {
		this.coins = coins;
	}

	public void subtractCoins(int coins) {
		this.coins -= coins;
	}

	public int getArmies() {
		return armies;
	}

	public void setArmies(int armies) {
		this.armies = armies;
	}

	public int getCities() {
		return cities;
	}

	public void setCities(int cities) {
		this.cities = cities;
	}

	public List<Card> getCards() {
		return cards;
	}

	public int getCardsNumber() {
		return cards.size();
	}

	public void addCard(Card card) {
		cards.add(card);
	}

	public boolean hasEndedGame(int numberOfPlayers) {
		if ((numberOfPlayers == 2 && cards.size() == 12)
				|| (numberOfPlayers == 3 && cards.size() == 10)
				|| (numberOfPlayers == 4 && cards.size() == 8)) {
			return true;
		}
		return false;
	}

	public int getResourcePoints() {
		int[] resources = new int[6];
		int resourcePts = 0;
		for (Card c : getCards()) {
			switch (c.getResource().getName()) {
			case ALL: {
				resources[0] += c.getResourceNumber();
				break;
			}
			case CARROT: {
				resources[1] += c.getResourceNumber();
				break;
			}
			case COAL: {
				resources[2] += c.getResourceNumber();
				break;
			}
			case RUBIN: {
				resources[3] += c.getResourceNumber();
				break;
			}
			case STEEL: {
				resources[4] += c.getResourceNumber();
				break;
			}
			case WOOD: {
				resources[5] += c.getResourceNumber();
				break;
			}
			}
		}

		for (ResourceName res : ResourceName.values()) {
			switch (res) {
			case ALL: {
				break;
			}
			case CARROT: {
				if (resources[1] == 3 || resources[1] == 4) {
					resourcePts += 1;
				} else if (resources[1] == 5 || resources[1] == 6) {
					resourcePts += 2;
				} else if (resources[1] == 7) {
					resourcePts += 3;
				} else if (resources[1] >= 8) {
					resourcePts += 5;
				}
				break;
			}
			case COAL: {
				if (resources[2] == 2) {
					resourcePts += 1;
				} else if (resources[2] == 3) {
					resourcePts += 2;
				} else if (resources[2] == 4) {
					resourcePts += 3;
				} else if (resources[2] >= 5) {
					resourcePts += 5;
				}
				break;
			}
			case RUBIN: {
				if (resources[3] == 1) {
					resourcePts += 1;
				} else if (resources[3] == 2) {
					resourcePts += 2;
				} else if (resources[3] == 3) {
					resourcePts += 3;
				} else if (resources[3] == 4) {
					resourcePts += 5;
				}
				break;
			}
			case STEEL: {
				if (resources[4] == 2 || resources[4] == 3) {
					resourcePts += 1;
				} else if (resources[4] == 4 || resources[4] == 5) {
					resourcePts += 2;
				} else if (resources[4] == 6) {
					resourcePts += 3;
				} else if (resources[4] >= 7) {
					resourcePts += 5;
				}
				break;
			}
			case WOOD: {
				if (resources[5] == 2 || resources[5] == 3) {
					resourcePts += 1;
				} else if (resources[5] == 4) {
					resourcePts += 2;
				} else if (resources[5] == 5) {
					resourcePts += 3;
				} else if (resources[5] >= 6) {
					resourcePts += 5;
				}
				break;
			}
			}
		}
		// matching card with all resources
		for (Card c : getCards()) {
			switch (c.getResource().getName()) {
			case ALL: {
				if (resources[1] == 7) {
					resources[1] += 1;
					resourcePts += 2;
				} else if (resources[2] == 4) {
					resources[2] += 1;
					resourcePts += 2;
				} else if (resources[3] == 3) {
					resources[3] += 1;
					resourcePts += 2;
				} else if (resources[4] == 6) {
					resources[4] += 1;
					resourcePts += 2;
				} else if (resources[5] == 5) {
					resources[5] += 1;
					resourcePts += 2;
				} else if (resources[1] == 2 || resources[1] == 4
						|| resources[1] == 6) {
					resources[1] += 1;
					resourcePts += 1;
				} else if (resources[2] == 1 || resources[2] == 2
						|| resources[2] == 3) {
					resources[2] += 1;
					resourcePts += 1;
				} else if (resources[3] == 0 || resources[3] == 1
						|| resources[3] == 2) {
					resources[3] += 1;
					resourcePts += 1;
				} else if (resources[4] == 1 || resources[4] == 3
						|| resources[4] == 5) {
					resources[4] += 1;
					resourcePts += 1;
				} else if (resources[5] == 1 || resources[5] == 3
						|| resources[5] == 4) {
					resources[5] += 1;
					resourcePts += 1;
				}
			}
			case CARROT:
			case COAL:
			case RUBIN:
			case STEEL:
			case WOOD:
				break;
			}
		}
		return resourcePts;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Player
				&& ((Player) object).getName() == this.name) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Player [" + name + ":" + coins + "]";
	}

	public Color getColor() {
		return color;
	}
}
