package model.dto;

import java.util.ArrayList;
import java.util.List;

public class TerritoriesConnection {

	private final Territory[] territories = new Territory[23];

	private final List<SingleConnection> connections = new ArrayList<SingleConnection>();

	public TerritoriesConnection() {
		for (int i = 0; i < territories.length; i++) {
			if (i < 9) {
				territories[i] = new Territory(i, 1);
			} else if (i < 12) {
				territories[i] = new Territory(i, 2);
			} else if (i < 14) {
				territories[i] = new Territory(i, 3);
			} else if (i < 16) {
				territories[i] = new Territory(i, 4);
			} else {
				territories[i] = new Territory(i, 5);
			}
		}
		connections.add(new SingleConnection(territories[0], territories[1],
				Connection.LAND));
		connections.add(new SingleConnection(territories[0], territories[16],
				Connection.SEA));
		connections.add(new SingleConnection(territories[0], territories[12],
				Connection.SEA));
		connections.add(new SingleConnection(territories[1], territories[2],
				Connection.LAND));
		connections.add(new SingleConnection(territories[1], territories[4],
				Connection.LAND));
		connections.add(new SingleConnection(territories[1], territories[5],
				Connection.LAND));
		connections.add(new SingleConnection(territories[2], territories[6],
				Connection.LAND));
		connections.add(new SingleConnection(territories[2], territories[5],
				Connection.LAND));
		connections.add(new SingleConnection(territories[2], territories[22],
				Connection.SEA));
		connections.add(new SingleConnection(territories[3], territories[4],
				Connection.LAND));
		connections.add(new SingleConnection(territories[3], territories[11],
				Connection.SEA));
		connections.add(new SingleConnection(territories[4], territories[5],
				Connection.LAND));
		connections.add(new SingleConnection(territories[4], territories[8],
				Connection.LAND));
		connections.add(new SingleConnection(territories[5], territories[6],
				Connection.LAND));
		connections.add(new SingleConnection(territories[5], territories[7],
				Connection.LAND));
		connections.add(new SingleConnection(territories[5], territories[8],
				Connection.LAND));
		connections.add(new SingleConnection(territories[6], territories[7],
				Connection.LAND));
		connections.add(new SingleConnection(territories[7], territories[8],
				Connection.LAND));
		connections.add(new SingleConnection(territories[9], territories[12],
				Connection.SEA));
		connections.add(new SingleConnection(territories[9], territories[10],
				Connection.LAND));
		connections.add(new SingleConnection(territories[10], territories[11],
				Connection.LAND));
		connections.add(new SingleConnection(territories[12], territories[13],
				Connection.LAND));
		connections.add(new SingleConnection(territories[13], territories[14],
				Connection.SEA));
		connections.add(new SingleConnection(territories[14], territories[15],
				Connection.LAND));
		connections.add(new SingleConnection(territories[15], territories[17],
				Connection.SEA));
		connections.add(new SingleConnection(territories[16], territories[17],
				Connection.LAND));
		connections.add(new SingleConnection(territories[16], territories[18],
				Connection.LAND));
		connections.add(new SingleConnection(territories[17], territories[18],
				Connection.LAND));
		connections.add(new SingleConnection(territories[17], territories[19],
				Connection.LAND));
		connections.add(new SingleConnection(territories[18], territories[19],
				Connection.LAND));
		connections.add(new SingleConnection(territories[18], territories[21],
				Connection.LAND));
		connections.add(new SingleConnection(territories[18], territories[22],
				Connection.LAND));
		connections.add(new SingleConnection(territories[19], territories[20],
				Connection.LAND));
		connections.add(new SingleConnection(territories[19], territories[21],
				Connection.LAND));
		connections.add(new SingleConnection(territories[20], territories[21],
				Connection.LAND));
		connections.add(new SingleConnection(territories[21], territories[22],
				Connection.LAND));
	}

	public Territory getTerritory(int number) {
		return territories[number];
	}

	public Territory[] getTerritories() {
		return territories;
	}

	public List<Territory> getNeighborsByLand(Territory territory) {
		List<Territory> neighbors = new ArrayList<Territory>();
		for (int i = 0; i < connections.size(); i++) {
			if (territory.getId() == connections.get(i).getX().getId()
					&& Connection.LAND.equals(connections.get(i).getType())) {
				neighbors.add(connections.get(i).getY());
			} else if (territory.getId() == connections.get(i).getY().getId()
					&& Connection.LAND.equals(connections.get(i).getType())) {
				neighbors.add(connections.get(i).getX());
			}
		}
		return neighbors;
	}

	public List<Territory> getNeighborsBySea(Territory territory) {
		List<Territory> neighbors = new ArrayList<Territory>();
		for (int i = 0; i < connections.size(); i++) {
			if (territory.getId() == connections.get(i).getX().getId()) {
				neighbors.add(connections.get(i).getY());
			} else if (territory.getId() == connections.get(i).getY().getId()) {
				neighbors.add(connections.get(i).getX());
			}
		}
		return neighbors;
	}

	class SingleConnection {
		private Territory x;
		private Territory y;
		Connection type;

		public SingleConnection(Territory x, Territory y, Connection type) {
			super();
			this.x = x;
			this.y = y;
			this.type = type;
		}

		public Territory getX() {
			return x;
		}

		public void setX(Territory x) {
			this.x = x;
		}

		public Territory getY() {
			return y;
		}

		public void setY(Territory y) {
			this.y = y;
		}

		public Connection getType() {
			return type;
		}

		public void setType(Connection type) {
			this.type = type;
		}

	}

	public enum Connection {
		LAND, SEA
	}
}
