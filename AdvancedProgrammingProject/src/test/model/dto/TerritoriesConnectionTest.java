package test.model.dto;

import java.util.List;

import model.dto.TerritoriesConnection;
import model.dto.Territory;

public class TerritoriesConnectionTest {

	public static void main(String[] args) {
		TerritoriesConnection connection = new TerritoriesConnection();
		Territory territory = connection.getTerritory(0);
		List<Territory> territories = connection.getNeighborsBySea(territory);
		if(territories.isEmpty()) {
			System.out.println("Empty list");
		}
		for(int i = 0; i < territories.size(); i++) {
			System.out.println(territories.get(i).getId());
		}
	}

}
