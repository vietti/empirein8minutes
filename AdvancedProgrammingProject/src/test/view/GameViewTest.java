package test.view;

import java.util.Map;

import model.Game;
import model.dto.BidEntry;
import model.maps.MapDataModel;
import view.GameView;
import view.GuiView;

public class GameViewTest {

	public static void main(String[] args) {
		Game game = new Game();
		
		MapDataModel map = new MapDataModel("../../resources/map/game-map.map");
		GuiView view = new GuiView(game, map);
		view.setVisible(true);
	}
}