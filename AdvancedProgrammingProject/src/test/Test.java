package test;

import model.Game;
import view.GameView;
import controller.GameController;

public class Test {
	public static void main(String[] args) {
		Game game = new Game();
		GameView gameView = new GameView(game);
		GameController controller = new GameController(game, gameView);
		controller.main();
	}
}
