package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;

import model.Game;
import model.dto.ActionName;
import model.dto.Army;
import model.dto.CardType;
import model.dto.City;
import model.dto.Territory;
import model.maps.MapDataModel;
import model.state.ArmyChosenState;
import model.state.CardChosenState;
import model.state.State;
import model.state.TerritoryChosenState;

public class MapPanel extends JPanel {

	Game game;
	MapDataModel map;
	private String overtext;
	MouseAdapter mouseAdapter;

	/**
	 * Create the panel.
	 */
	public MapPanel() {

	}

	public MapPanel(Game game, MapDataModel map) {
		this.game = game;
		this.map = map;
		registerListeners();
		setMinimumSize(new Dimension(500, 400));
	}

	final private void registerListeners() {

		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent ev) {
				String over = map.getRegion(ev.getPoint());
				if (over != null) {
					overtext = "Region " + over;
					repaint();
				}
			}
		});

	}

	public void paintComponent(Graphics gr) {

		super.paintComponent(gr);
		Graphics2D g = (Graphics2D) gr;

		if (map == null)
			return;
		Image img = map.getMapBackground();
		if (img != null)
			g.drawImage(img, 0, 0, null);

		if (overtext != null) {
			Font f = g.getFont();
			g.setFont(new Font("Roman", Font.BOLD, 16));
			g.drawString(overtext, 10, getHeight()
					- g.getFontMetrics(f).getHeight() - 10);
			g.setFont(f);
		}
		for (Shape a : map.getRegions()) {
			Graphics2D g2d = (Graphics2D) g;
			String name = map.getAreaName(a);
			int number = Integer.parseInt(name);
			Territory territory = game.getTerritory(number);
			g2d.setColor(Color.GREEN);

			if (!game.getAllowedTerritories().isEmpty()
					&& game.getAllowedTerritories().contains(territory)) {
				g2d.fill(a);
			} else if (!game.getAllowedArmies().isEmpty()) {
				List<Army> armies = game.getAllowedArmies();
				for (int i = 0; i < armies.size(); i++) {
					if (armies.get(i).getPlace().equals(territory)) {
						g2d.fill(a);
					}
				}
			}

			Rectangle2D r = a.getBounds2D();
			int cx = (int) (r.getCenterX());
			int cy = (int) (r.getCenterY());
			List<Army> armies = game.getArmiesForTerritory(territory);
			List<City> cities = game.getCitiesForTerritory(territory);
			int outOfTerritory = 0;
			for (int i = 0; i < armies.size(); i++) {
				g.setColor(armies.get(i).getOwner().getColor());
				int placeX = (cx - ((cx - (int) r.getMinX()) / 2) + i * 8);
				int placeY = cy - ((cy - (int) r.getMinY()) / 10);
				if (placeX > r.getMaxX()) {
					if (outOfTerritory == 0) {
						outOfTerritory = i;
					}
					placeX = (cx - ((cx - (int) r.getMinX()) / 2) + (i - outOfTerritory) * 8);
					placeY += 8;
				}
				g.fillRect(placeX, placeY, 5, 5);
			}

			for (int i = 0; i < cities.size(); i++) {
				g.setColor(cities.get(i).getOwner().getColor());
				int placeX = (cx - ((cx - (int) r.getMinX()) / 2) + i * 12);
				int placeY = cy + (((int) r.getMaxY() - cy) / 10);
				if (placeX > r.getMaxX()) {
					if (outOfTerritory == 0) {
						outOfTerritory = i;
					}
					placeX = (cx - ((cx - (int) r.getMinX()) / 2) + (i - outOfTerritory) * 12);
					placeY += 8;
				}
				g.fillOval(placeX, placeY, 15, 15);
			}
		}
	}

	public void update(State state, CardType type, ActionName action) {
		repaint();
		if (state instanceof CardChosenState) {
			if (CardType.TO_CHOOSE.equals(type)
					&& game.isChosenAction() == false) {
				ChooseActionFrame ChooseActionFrame = new ChooseActionFrame(
						game);
				ChooseActionFrame.setVisible(true);
			} else {
				switch (action) {
				case DESTROY_ARMY:
					registerListenersForArmyDestroy();
					break;
				case MOVE_ARMY:
					registerListenersForArmy();
					break;
				case MOVE_ARMY_BY_SEA:
					registerListenersForArmy();
					break;
				case PLACE_CITY:
					registerListenersForMove();
					break;
				case PLACE_ARMY:
					registerListenersForMove();
					break;
				}
			}
		} else if (state instanceof ArmyChosenState) {
			switch (action) {
			case MOVE_ARMY:
				registerListenersForMove();
				break;
			case MOVE_ARMY_BY_SEA:
				registerListenersForMove();
				break;
			case DESTROY_ARMY:
				registerListenersForArmyDestroy();
				break;
			default:
				System.err.println("Impossible: " + action + ", " + type);
				break;
			}
		} else if (state instanceof TerritoryChosenState) {
			switch (action) {
			case MOVE_ARMY:
				registerListenersForArmy();
				break;
			case MOVE_ARMY_BY_SEA:
				registerListenersForArmy();
				break;
			case PLACE_CITY:
				registerListenersForMove();
				break;
			case PLACE_ARMY:
				registerListenersForMove();
				break;
			default:
				System.err.println("Impossible: " + action + ", " + type);
				break;
			}
		}
	}

	private void registerListenersForMove() {
		if (mouseAdapter != null) {
			removeMouseListener(mouseAdapter);
		}
		mouseAdapter = new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				String region = map.getRegion(e.getPoint());
				if (region != null) {
					int number = Integer.parseInt(region);
					List<Territory> territories = game.getAllowedTerritories();
					Territory territory = game.getTerritory(number);
					if (territories.contains(territory)) {
						game.chooseTerritory(territory);
					}
				}
			}
		};
		addMouseListener(mouseAdapter);
	}

	private void registerListenersForArmy() {
		if (mouseAdapter != null) {
			removeMouseListener(mouseAdapter);
		}
		mouseAdapter = new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				String region = map.getRegion(e.getPoint());
				if (region != null) {
					int number = Integer.parseInt(region);
					List<Army> armies = game.getAllowedArmies();
					Territory territory = game.getTerritory(number);
					for (int i = 0; i < armies.size(); i++) {
						if (armies.get(i).getPlace().equals(territory)) {
							game.chooseArmy(armies.get(i));
						}
					}
				}
			}
		};
		addMouseListener(mouseAdapter);
	}

	private void registerListenersForArmyDestroy() {
		if (mouseAdapter != null) {
			removeMouseListener(mouseAdapter);
		}
		mouseAdapter = new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				String region = map.getRegion(e.getPoint());
				if (region != null) {
					int number = Integer.parseInt(region);
					List<Territory> territories = game
							.getAllowedArmiesTerritories();
					Territory territory = game.getTerritory(number);
					if (territories.contains(territory)) {
						List<Army> enemyArmies = game
								.getRivalsArmiesForTerritory(territory);
						ChooseArmyFrame frame = new ChooseArmyFrame(game,
								enemyArmies);
						frame.setVisible(true);
					}

				}
			}
		};
		addMouseListener(mouseAdapter);
	}
}
