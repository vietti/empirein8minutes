package view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Game;
import model.dto.ActionName;
import model.dto.Army;
import model.dto.Card;
import model.dto.CardType;
import model.dto.Player;
import model.dto.Territory;
import model.maps.MapDataModel;
import model.state.ArmyChosenState;
import model.state.BeforeChoosingCardState;
import model.state.BiddingState;
import model.state.CardChosenState;
import model.state.StartGameState;
import model.state.State;
import model.state.TerritoryChosenState;

public class GuiView extends JFrame implements View {

	Game gm;
	MapDataModel mm;
	StartPanel startPanel;
	BidPanel bidPanel;
	GamePanel gamePanel;
	GameStatsFrame frame;

	public GuiView(Game gm, MapDataModel mm) {
		this.gm = gm;
		this.mm = mm;
		gm.registerView(this);
		update(null, null);
		setSize(900, 1000);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private static final long serialVersionUID = -7180177805528823682L;

	@Override
	public void update(Observable o, Object arg) {
		Container c = getContentPane();
		c.setLayout(new BorderLayout());
		State state = gm.getState();
		if (state instanceof StartGameState) {
			if (gm.isGameFinished() && frame == null) {
				Map<Player, Integer> stats = gm.getResults();
				frame = new GameStatsFrame(stats);
				frame.setVisible(true);
				return;
			}
			if (gamePanel != null) {
				gamePanel.removeAll();
				c.remove(gamePanel);
				revalidate();
			}
			startPanel = new StartPanel(gm);
			c.add(startPanel, BorderLayout.CENTER);
		} else if (state instanceof BiddingState) {
			if (bidPanel == null) {
				startPanel.removeAll();
				c.remove(startPanel);
				revalidate();
				bidPanel = new BidPanel(gm);
				c.add(bidPanel, BorderLayout.CENTER);
				bidPanel.update();
				revalidate();
			} else {
				bidPanel.update();
			}

		} else if (state instanceof BeforeChoosingCardState) {
			if (gamePanel == null) {
				bidPanel.removeAll();
				c.remove(bidPanel);
				revalidate();
				gamePanel = new GamePanel(gm, mm);
				c.add(gamePanel, BorderLayout.CENTER);
				revalidate();
			} else {
				gamePanel.update(state);
			}
		} else {
			Card card = gm.getCurrentCard();
			gamePanel.update(state, card.getType(), card.getActionName());
		}

	}

	@Override
	public void update() {
		update(null, null);
	}

}
