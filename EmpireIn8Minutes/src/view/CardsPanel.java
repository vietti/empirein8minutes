package view;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.GrayFilter;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;

import model.Game;
import model.dto.Card;
import model.dto.CardType;

public class CardsPanel extends JPanel {

	Game game;
	JButton[] btns;
	GridBagConstraints c;
	JLabel[] actions;

	/**
	 * Create the panel.
	 * 
	 * @param gm
	 */
	public CardsPanel(Game gm) {
		this.game = gm;
		setLayout(new GridBagLayout());
		c = new GridBagConstraints();
		addCards();
	}

	private void addCards() {
		btns = new JButton[6];
		actions = new JLabel[6];
		Card[] allowedCards = game.getAllowedCards();
		Card[] cards = game.getCards();
		for (int i = 0; i < btns.length; i++) {
			btns[i] = new JButton();
			try {
				String prefix = "";
				if(cards[i].getResourceNumber() == 2) {
					prefix = "double_";
				}
				Image img = ImageIO.read(getClass().getResource(
						"../resources/cards/" + prefix + cards[i].getResource().getName().toString().toLowerCase() + ".gif"));
				if (allowedCards[i] != null) {
					btns[i].setIcon(new ImageIcon(img));
					btns[i].addActionListener(new CardActionListener(i, game));
				} else {
					btns[i].setIcon(getGray(new ImageIcon(img)));
				}
				btns[i].setBorderPainted(false);
				btns[i].setContentAreaFilled(false);
				btns[i].setFocusPainted(false);
				btns[i].setOpaque(false);
				btns[i].setMargin(new Insets(0, 0, 0, 0));
			} catch (IOException e) {
				e.printStackTrace();
			}
			c.fill = GridBagConstraints.BOTH;
			c.gridx = i;
			c.gridy = 0;
			add(btns[i], c);
			actions[i] = new JLabel();
			String text = "<html>Action: " + cards[i].getFirstActionInString() + " - " + cards[i].getActionPoints() + " time(s)";
			if(CardType.DOUBLE.equals(cards[i].getType())) {
				text += " AND " + cards[i].getSecondActionInString() + " - " + cards[i].getSecondActionPoints() + " time(s)";
			} else if(CardType.TO_CHOOSE.equals(cards[i].getType())) {
				text += " OR " + cards[i].getSecondActionInString() + " - " + cards[i].getSecondActionPoints() + " time(s)";			
			} else {
				text += "</html>";
			}
			actions[i].setText(text);
			c.fill = GridBagConstraints.BOTH;
			c.gridx = i;
			c.gridy = 1;
			add(actions[i],c);
		}
	}

	public void removeActionListeners() {
		for (int i = 0; i < btns.length; i++) {
		    for( ActionListener al : btns[i].getActionListeners() ) {
		    	btns[i].removeActionListener( al );
		    }
		}
	}
	
	private void removeCards() {
		for (int i = 0; i < btns.length; i++) {
        remove(btns[i]);
        remove(actions[i]);
        revalidate();
        repaint();
		}
	}
	
	private Icon getGray(Icon icon) {
		final int w = icon.getIconWidth();
		final int h = icon.getIconHeight();
		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		GraphicsDevice gd = ge.getDefaultScreenDevice();
		GraphicsConfiguration gc = gd.getDefaultConfiguration();
		BufferedImage image = gc.createCompatibleImage(w, h);
		Graphics2D g2d = image.createGraphics();
		icon.paintIcon(null, g2d, 0, 0);
		Image gray = GrayFilter.createDisabledImage(image);
		return new ImageIcon(gray);
	}

	class CardActionListener implements ActionListener {
		private final int cardNum;
		private final Game game;

		CardActionListener(final int cardNum, final Game game) {
			super();
			this.cardNum = cardNum;
			this.game = game;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			game.chooseCard(cardNum);
		}
	}
	
	public void update() {
		removeCards();
		addCards();
	}
	
}
