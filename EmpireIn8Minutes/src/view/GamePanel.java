package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JPanel;

import model.Game;
import model.dto.ActionName;
import model.dto.Card;
import model.dto.CardType;
import model.dto.ResourceName;
import model.maps.MapDataModel;
import model.state.ArmyChosenState;
import model.state.BeforeChoosingCardState;
import model.state.CardChosenState;
import model.state.State;
import model.state.TerritoryChosenState;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;

public class GamePanel extends JPanel {

	Game game;
	MapDataModel map;
	CardsPanel cardsPanel;
	MapPanel mapPanel;
	JLabel lblNrfood;
	JLabel lblNrcoal;
	JLabel lblNrwood;
	JLabel lblNrrubin;
	JLabel lblNrall;
	JLabel lblNrsteel;
	JButton btnResignFromAction;
	private JLabel lblYouHaveStill;
	private JLabel lblNrcoins;
	private JLabel lblCoinsLeft;
	private JLabel lblCityTokensLeft;
	private JLabel lblArmyTokensLeft;
	private JLabel lblNrcity;
	private JLabel lblNrarmy;
	private JLabel lblTurnOfPlayer;
	private JLabel lblPlayername;

	/**
	 * Create the panel.
	 * 
	 * @param gm
	 */
	public GamePanel(Game game, MapDataModel map) {
		this.game = game;
		this.map = map;
		setLayout(null);

		cardsPanel = new CardsPanel(game);
		cardsPanel.setBounds(12, 12, 790, 308);
		add(cardsPanel);

		mapPanel = new MapPanel(game, map);
		mapPanel.setBounds(22, 332, 556, 383);
		add(mapPanel);
		
		addCardsLabels();
		addTokenLabels();
		btnResignFromAction = new JButton("Resign from action");
		btnResignFromAction.setBounds(611, 678, 191, 25);
		btnResignFromAction.addActionListener(new ResignActionListener(game));
		add(btnResignFromAction);
		
		lblTurnOfPlayer = new JLabel("turn.");
		lblTurnOfPlayer.setBounds(596, 354, 206, 15);
		add(lblTurnOfPlayer);
		
		lblPlayername = new JLabel(game.getCurrentPlayer().getName() + "'s");
		lblPlayername.setForeground(game.getCurrentPlayer().getColor());
		lblPlayername.setFont(new Font("Dialog", Font.BOLD, 16));
		lblPlayername.setBounds(596, 332, 200, 15);
		add(lblPlayername);
		
		btnResignFromAction.setVisible(false);
	}
	
	private void addTokenLabels() {
		lblYouHaveStill = new JLabel("You have still:");
		lblYouHaveStill.setFont(new Font("Dialog", Font.BOLD, 14));
		lblYouHaveStill.setBounds(596, 570, 137, 15);
		add(lblYouHaveStill);
		
		lblNrcoins = new JLabel(Integer.toString(game.getCurrentPlayer().getCoins()));
		lblNrcoins.setBounds(596, 597, 70, 15);
		add(lblNrcoins);
		
		lblCoinsLeft = new JLabel("coins left");
		lblCoinsLeft.setBounds(640, 597, 70, 15);
		add(lblCoinsLeft);
		
		lblCityTokensLeft = new JLabel("city tokens left");
		lblCityTokensLeft.setBounds(640, 624, 121, 15);
		add(lblCityTokensLeft);
		
		lblArmyTokensLeft = new JLabel("army tokens left");
		lblArmyTokensLeft.setBounds(640, 651, 121, 15);
		add(lblArmyTokensLeft);
		
		lblNrcity = new JLabel(Integer.toString(game.getCurrentPlayer().getCities()));
		lblNrcity.setBounds(596, 624, 70, 15);
		add(lblNrcity);
		
		lblNrarmy = new JLabel(Integer.toString(game.getCurrentPlayer().getArmies()));
		lblNrarmy.setBounds(596, 651, 70, 15);
		add(lblNrarmy);
	}
	
	private void addCardsLabels() {
		JLabel lblYourCards = new JLabel("Your cards:");
		lblYourCards.setFont(new Font("Dialog", Font.BOLD, 14));
		lblYourCards.setBounds(596, 381, 137, 15);
		add(lblYourCards);
		
		JLabel lblFoodCards = new JLabel("food cards");
		lblFoodCards.setBounds(640, 408, 121, 15);
		add(lblFoodCards);
		
		JLabel lblWoodCards = new JLabel("wood cards");
		lblWoodCards.setBounds(640, 435, 103, 15);
		add(lblWoodCards);
		
		JLabel lblSteelCards = new JLabel("steel cards");
		lblSteelCards.setBounds(640, 462, 129, 15);
		add(lblSteelCards);
		
		JLabel lblCoalCards = new JLabel("coal cards");
		lblCoalCards.setBounds(640, 489, 103, 15);
		add(lblCoalCards);
		
		JLabel lblRubinCards = new JLabel("rubin cards");
		lblRubinCards.setBounds(640, 516, 103, 15);
		add(lblRubinCards);
		
		lblNrfood = new JLabel("0");
		lblNrfood.setBounds(596, 408, 70, 15);
		add(lblNrfood);
		
		lblNrwood = new JLabel("0");
		lblNrwood.setBounds(596, 435, 70, 15);
		add(lblNrwood);
		
		lblNrsteel = new JLabel("0");
		lblNrsteel.setBounds(596, 462, 70, 15);
		add(lblNrsteel);
		
		lblNrcoal = new JLabel("0");
		lblNrcoal.setBounds(596, 489, 70, 15);
		add(lblNrcoal);
		
		lblNrrubin = new JLabel("0");
		lblNrrubin.setBounds(596, 516, 70, 15);
		add(lblNrrubin);
		
		JLabel lblAnyResourceCard = new JLabel("any resource card");
		lblAnyResourceCard.setBounds(640, 543, 146, 15);
		add(lblAnyResourceCard);
		
		lblNrall = new JLabel("0");
		lblNrall.setBounds(596, 543, 70, 15);
		add(lblNrall);
	}

	public void update(State state, CardType type, ActionName action) {
		btnResignFromAction.setVisible(! (state instanceof BeforeChoosingCardState) );
		mapPanel.update(state, type, action);
	}
	
	public void update(State state) {
		mapPanel.repaint();
		btnResignFromAction.setVisible(! (state instanceof BeforeChoosingCardState) );
		if(state instanceof BeforeChoosingCardState) {
			List<Card> cards = game.getCurrentPlayer().getCards();
			int coal = 0, food = 0, steel = 0, wood = 0, rubin = 0, all = 0;
			for(int i = 0; i < cards.size(); i++) {
				if(ResourceName.ALL.equals(cards.get(i).getResource().getName())) {
					all++;
				} else if(ResourceName.CARROT.equals(cards.get(i).getResource().getName())) {
					food++;
				} else if(ResourceName.COAL.equals(cards.get(i).getResource().getName())) {
					coal++;
				} else if(ResourceName.RUBIN.equals(cards.get(i).getResource().getName())) {
					rubin++;
				} else if(ResourceName.STEEL.equals(cards.get(i).getResource().getName())) {
					steel++;
				} else if(ResourceName.WOOD.equals(cards.get(i).getResource().getName())) {
					wood++;
				}
			}
			lblNrfood.setText(Integer.toString(food));
			lblNrall.setText(Integer.toString(all));
			lblNrcoal.setText(Integer.toString(coal));
			lblNrrubin.setText(Integer.toString(rubin));
			lblNrwood.setText(Integer.toString(wood));
			lblNrsteel.setText(Integer.toString(steel));
			lblNrarmy.setText(Integer.toString(game.getCurrentPlayer().getArmies()));
			lblNrcity.setText(Integer.toString(game.getCurrentPlayer().getCities()));
			lblNrcoins.setText(Integer.toString(game.getCurrentPlayer().getCoins()));
			lblPlayername.setText(game.getCurrentPlayer().getName() + "'s");
			lblPlayername.setForeground(game.getCurrentPlayer().getColor());
			cardsPanel.update();
		}
	}
	
	class ResignActionListener implements ActionListener {
		private final Game game;

		ResignActionListener(final Game game) {
			super();
			this.game = game;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			game.resignFromAction();
		}
	}
}
