package model;

import java.util.ArrayList;
import java.util.List;

import view.View;

public class Model {
	private List<View> views;
	private boolean changed;

	public Model() {
		views = new ArrayList<View>();
		changed = true;
	}

	public void registerView(View view) {
		if (view == null)
			throw new NullPointerException();
		if (!views.contains(view))
			views.add(view);
	}

	public void notifyViews() {
		if (changed)
			for (View view : views)
				view.update();
		changed = false;
	}

	public void setChanged() {
		changed = true;
	}

	public boolean getChanged() {
		return changed;
	}
}
