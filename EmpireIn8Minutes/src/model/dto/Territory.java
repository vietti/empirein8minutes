package model.dto;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import model.Game;
import utils.MapUtils;

public class Territory {
	private int id;
	private int continent;

	public Territory(int id, int continent) {
		super();
		this.id = id;
		this.continent = continent;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getContinent() {
		return continent;
	}

	public void setContinent(int continent) {
		this.continent = continent;
	}

	public Player getTerritoryController(Game game) {
		Map<Player, Integer> areasAndCitiesOnTerritory = new HashMap<Player, Integer>();
		for (Player p : game.getPlayers()) {
			areasAndCitiesOnTerritory.put(p, 0);
		}
		for (Army a : game.getArmies()) {
			if (a.getPlace().equals(this)) {
				areasAndCitiesOnTerritory.put(a.getOwner(),
						areasAndCitiesOnTerritory.get(a.getOwner()) + 1);
			}
		}
		for (City c : game.getCities()) {
			if (c.getPlace().equals(this)) {
				areasAndCitiesOnTerritory.put(c.getOwner(),
						areasAndCitiesOnTerritory.get(c.getOwner()) + 1);
			}
		}
		areasAndCitiesOnTerritory = MapUtils
				.sortByValue(areasAndCitiesOnTerritory);
		Iterator<Map.Entry<Player, Integer>> iterator = areasAndCitiesOnTerritory
				.entrySet().iterator();
		Map.Entry<Player, Integer> firstPlayer = iterator.next();
		Map.Entry<Player, Integer> secondPlayer = iterator.next();
		if (firstPlayer.getValue().equals(secondPlayer.getValue()))
			return null;
		else
			return firstPlayer.getKey();

	}

	public static Player getContinentController(int continentId, Game game) {
		Map<Player, Integer> territoriesOnContinent = new HashMap<Player, Integer>();
		for (Player p : game.getPlayers()) {
			territoriesOnContinent.put(p, 0);
		}
		for (Map.Entry<Territory, Player> tc : game.getTerritoryControllers()
				.entrySet()) {
			if (tc.getKey().getContinent() == continentId) {
				territoriesOnContinent.put(tc.getValue(),
						territoriesOnContinent.get(tc.getValue()) + 1);
			}
		}
		territoriesOnContinent = MapUtils.sortByValue(territoriesOnContinent);
		Iterator<Map.Entry<Player, Integer>> iterator = territoriesOnContinent
				.entrySet().iterator();
		Map.Entry<Player, Integer> firstPlayer = iterator.next();
		Map.Entry<Player, Integer> secondPlayer = iterator.next();
		if (firstPlayer.getValue().equals(secondPlayer.getValue()))
			return null;
		else
			return firstPlayer.getKey();
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Territory
				&& ((Territory) object).getId() == this.id) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Territory:" + id;
	}

}
