package model.dto;

public class City {
	private Territory place;
	private Player owner;

	public City(Player owner, Territory place) {
		this.owner = owner;
		this.place = place;
	}

	public Territory getPlace() {
		return place;
	}

	public void setPlace(Territory place) {
		this.place = place;
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "City [Owner: " + owner + ", territory: " + place + "]\n";
	}
}
