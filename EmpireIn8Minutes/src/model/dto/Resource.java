package model.dto;

import java.util.LinkedHashMap;

public class Resource {
	private ResourceName name;
	private LinkedHashMap<Integer, Integer> points;

	public ResourceName getName() {
		return name;
	}

	public void setName(ResourceName name) {
		this.name = name;
	}

	public LinkedHashMap<Integer, Integer> getPoints() {
		return points;
	}

	public void setPoints(LinkedHashMap<Integer, Integer> points) {
		this.points = points;
	}

	public Resource(ResourceName name) {
		this.name = name;
		this.points = new LinkedHashMap<Integer, Integer>();
		if (ResourceName.CARROT.equals(name)) {
			this.points.put(1, 3);
			this.points.put(2, 5);
			this.points.put(3, 7);
			this.points.put(5, 8);
		} else if (ResourceName.COAL.equals(name)) {
			this.points.put(1, 2);
			this.points.put(2, 3);
			this.points.put(3, 4);
			this.points.put(5, 5);
		} else if (ResourceName.RUBIN.equals(name)) {
			this.points.put(1, 1);
			this.points.put(2, 2);
			this.points.put(3, 3);
			this.points.put(5, 4);
		} else if (ResourceName.STEEL.equals(name)) {
			this.points.put(1, 2);
			this.points.put(2, 4);
			this.points.put(3, 6);
			this.points.put(5, 7);
		} else if (ResourceName.WOOD.equals(name)) {
			this.points.put(1, 2);
			this.points.put(2, 4);
			this.points.put(3, 5);
			this.points.put(5, 6);
		}
	}

	@Override
	public String toString() {
		return name.toString();
	}

	int gentPointsForCards(int n) {
		if (points.get(1) < n) {
			return 0;
		} else if (points.get(2) < n) {
			return 2;
		} else if (points.get(3) < n) {
			return 3;
		}
		return 0;
	}
}
