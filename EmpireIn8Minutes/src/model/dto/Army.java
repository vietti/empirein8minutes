package model.dto;

public class Army {
	private Territory place;
	private Player owner;

	public Army(Player owner, Territory place) {
		this.owner = owner;
		this.place = place;
	}

	public Territory getPlace() {
		return place;
	}

	public void setPlace(Territory place) {
		this.place = place;
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Army && ((Army) object).getPlace() == this.place
				&& ((Army) object).getOwner() == this.owner) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Army [Owner: " + owner + ", territory: " + place + "]\n";
	}
}
