package model.state;

import model.Game;
import model.dto.ActionName;
import model.dto.Army;
import model.dto.Card;
import model.dto.CardType;
import model.dto.Territory;

public class BeforeChoosingCardState implements State {
	Game game;

	public BeforeChoosingCardState(Game game) {
		this.game = game;
	}

	@Override
	public void defineNumbersOfPlayers(int number) {
		// TODO Auto-generated method stub

	}

	@Override
	public void bid(String playerName, int amount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseCard(int position) {
		Card chosenCard = game.getCardAtPosition(position);
		int price = game.getPriceAtPosition(position);
		game.setCurrentCard(chosenCard);
		game.getCurrentPlayer().addCard(chosenCard);
		game.getCurrentPlayer().subtractCoins(price);
		game.setTwoActions(false);
		game.setSecondAction(false);
		if (!CardType.TO_CHOOSE.equals(chosenCard.getType())) {
			game.filterAllowedArmies(chosenCard.getActionName());
			game.filterAllowedTerritories(chosenCard.getActionName());
			game.setCredits(chosenCard.getActionPoints());
			if (CardType.DOUBLE.equals(chosenCard.getType())) {
				game.setTwoActions(true);
			}
		}
		game.setState(game.getCardChosenState());
		// game.fillTable(); and setPlayer after taking action
		// set AllowedCards tez?
	}

	@Override
	public void chooseAction(ActionName action) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseArmy(Army army) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseTerritory(Territory territory) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resignFromAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return this.getClass().getName();
	}
}
