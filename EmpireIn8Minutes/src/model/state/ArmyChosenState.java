package model.state;

import model.Game;
import model.dto.ActionName;
import model.dto.Army;
import model.dto.Territory;

public class ArmyChosenState implements State {
	Game game;

	public ArmyChosenState(Game game) {
		this.game = game;
	}

	@Override
	public void defineNumbersOfPlayers(int number) {
		// TODO Auto-generated method stub

	}

	@Override
	public void bid(String playerName, int amount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseCard(int position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseAction(ActionName action) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseArmy(Army army) {
		ActionName action = game.getCurrentCard().getActionName();
		switch (action) {
		case DESTROY_ARMY: {
			game.removeArmy(army);
			// game.unsetAllowedArmies();
			game.filterAllowedArmies(action);
			game.subtractCredits(1);
			break;
		}
		default:
			break;
		}
		if (game.checkIfCreditsUsed()) {
			game.unsetAllowedArmies();
			game.unsetAllowedTerritories();
			game.nextTurn();
		} else
			game.setState(game.getArmyChosenState());
	}

	@Override
	public void chooseTerritory(Territory territory) {
		ActionName action = game.getCurrentCard().getActionName();
		switch (action) {
		case MOVE_ARMY: {
			game.unsetAllowedTerritories();
			Army armyToMove = game.getChosenArmy();
			game.removeArmy(armyToMove);
			armyToMove.setPlace(territory);
			game.addArmy(armyToMove);
			game.setChosenArmy(null);
			game.subtractCredits(1);
			game.filterAllowedArmies(action);
			break;
		}
		case MOVE_ARMY_BY_SEA: {
			game.unsetAllowedTerritories();
			Army armyToMove = game.getChosenArmy();
			game.removeArmy(armyToMove);
			armyToMove.setPlace(territory);
			game.addArmy(armyToMove);
			game.setChosenArmy(null);
			game.subtractCredits(1);
			game.filterAllowedArmies(action);
			break;
		}
		default:
			break;
		}
		if (game.checkIfCreditsUsed()) {
			game.unsetAllowedArmies();
			game.unsetAllowedTerritories();
			game.nextTurn();
		} else
			game.setState(game.getTerritoryChosenState());
	}

	@Override
	public void resignFromAction() {
		game.unsetAllowedArmies();
		game.unsetAllowedTerritories();
		game.nextTurn();

	}

	@Override
	public String toString() {
		return this.getClass().getName();
	}

}
