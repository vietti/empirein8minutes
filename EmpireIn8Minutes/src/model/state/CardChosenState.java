package model.state;

import model.Game;
import model.dto.ActionName;
import model.dto.Army;
import model.dto.Card;
import model.dto.City;
import model.dto.Territory;

public class CardChosenState implements State {
	Game game;

	public CardChosenState(Game game) {
		this.game = game;
	}

	@Override
	public void defineNumbersOfPlayers(int number) {
		// TODO Auto-generated method stub

	}

	@Override
	public void bid(String playerName, int amount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseCard(int position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseAction(ActionName action) {
		Card card = game.getCurrentCard();
		card.setActionAsFirst(action);
		game.filterAllowedArmies(card.getActionName());
		game.filterAllowedTerritories(card.getActionName());
		game.setCredits(card.getActionPoints());
		game.setChosenAction(true);
	}

	@Override
	public void chooseArmy(Army army) {
		ActionName action = game.getCurrentCard().getActionName();
		switch (action) {
		case MOVE_ARMY: {
			game.setChosenArmy(army);
			game.unsetAllowedArmies();
			game.filterAllowedTerritories(action);
			break;
		}
		case MOVE_ARMY_BY_SEA: {
			game.setChosenArmy(army);
			game.unsetAllowedArmies();
			game.filterAllowedTerritories(action);
			break;
		}
		case DESTROY_ARMY: {
			game.removeArmy(army);
			game.filterAllowedArmies(action);
			// game.unsetAllowedArmies();
			game.subtractCredits(1);
			break;
		}
		default:
			break;
		}
		if (game.checkIfCreditsUsed()) {
			game.unsetAllowedArmies();
			game.unsetAllowedTerritories();
			game.nextTurn();
		} else
			game.setState(game.getArmyChosenState());
	}

	@Override
	public void chooseTerritory(Territory territory) {
		ActionName action = game.getCurrentCard().getActionName();
		switch (action) {
		case PLACE_ARMY: {
			Army newArmy = new Army(game.getCurrentPlayer(), territory);
			game.addArmy(newArmy);
			game.filterAllowedTerritories(action);
			game.subtractCredits(1);
			break;
		}
		case PLACE_CITY: {
			City newCity = new City(game.getCurrentPlayer(), territory);
			game.addCity(newCity);
			game.filterAllowedTerritories(action);
			game.subtractCredits(1);
			break;
		}
		default:
			break;
		}
		if (game.checkIfCreditsUsed()) {
			game.unsetAllowedArmies();
			game.unsetAllowedTerritories();
			game.nextTurn();
		} else
			game.setState(game.getTerritoryChosenState());
	}

	@Override
	public void resignFromAction() {
		game.unsetAllowedArmies();
		game.unsetAllowedTerritories();
		game.nextTurn();
	}

	@Override
	public String toString() {
		return this.getClass().getName();
	}

}
