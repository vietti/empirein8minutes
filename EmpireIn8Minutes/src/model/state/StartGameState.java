package model.state;

import model.Game;
import model.dto.ActionName;
import model.dto.Army;
import model.dto.Territory;

public class StartGameState implements State {
	Game game;

	public StartGameState(Game game) {
		this.game = game;
	}

	@Override
	public void defineNumbersOfPlayers(int number) {
		game.setPlayersNumber(number);
		game.createTable();
		game.setState(game.getBiddingState());
	}

	@Override
	public void bid(String playerName, int amount) {
		// TODO chuj tam
	}

	@Override
	public void chooseCard(int position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseAction(ActionName action) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseArmy(Army army) {
		// TODO Auto-generated method stub

	}

	@Override
	public void chooseTerritory(Territory territory) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resignFromAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return this.getClass().getName();
	}
}
