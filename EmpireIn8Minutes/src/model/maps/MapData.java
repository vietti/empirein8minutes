package model.maps;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class MapData extends RegionData implements IMapData {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	transient Image mapBackground;
	String mapBackgroundName;

	// File mapBackgroundFile;
	// URL mapBackgroundURL;

	public void setMapBackground(File f) {
		mapBackgroundName = f.getName();
		System.out.println("Map background name: " + mapBackgroundName);
		/*
		 * mapBackgroundFile=f; try { mapBackgroundURL=f.toURI().toURL(); //see
		 * toURL javadoc for explanation } catch (MalformedURLException e) {
		 * e.printStackTrace(); }
		 */
		mapBackground = null;
	}

	/*
	 * public void setMapBackground(URL f){ mapBackgroundURL=f;
	 * mapBackground=null; }
	 */

	@Override
	public Image getMapBackground() {

		if (mapBackground == null && (mapBackgroundName != null))
			try {
				System.out.println("opening map " + mapBackgroundName);
				URL imgURL = Resources.getResourceFile("../../resources/images/"
						+ mapBackgroundName);

				mapBackground = ImageIO.read(imgURL);
			} catch (IOException e) {
				e.printStackTrace();
			}
		return mapBackground;
	}
}
