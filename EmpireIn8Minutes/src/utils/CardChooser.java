package utils;

import java.util.HashMap;
import java.util.Random;

import model.dto.ActionName;
import model.dto.Card;
import model.dto.CardType;
import model.dto.ResourceName;

public class CardChooser {
	private HashMap<ResourceName, Integer> numOfCards;
	private HashMap<ActionName, Integer> actions;
	private Random random = new Random();
	int doubleCards;
	int toChooseCards;

	public CardChooser() {
		this.doubleCards = 1;
		this.toChooseCards = 5;
		numOfCards = new HashMap<ResourceName, Integer>();
		actions = new HashMap<ActionName, Integer>();
		this.numOfCards.put(ResourceName.ALL, 3);
		this.numOfCards.put(ResourceName.CARROT, 10);
		this.numOfCards.put(ResourceName.COAL, 7);
		this.numOfCards.put(ResourceName.RUBIN, 5);
		this.numOfCards.put(ResourceName.STEEL, 9);
		this.numOfCards.put(ResourceName.WOOD, 8);
		this.actions.put(ActionName.DESTROY_ARMY, 1);
		this.actions.put(ActionName.MOVE_ARMY, 14);
		this.actions.put(ActionName.MOVE_ARMY_BY_SEA, 9);
		this.actions.put(ActionName.PLACE_ARMY, 17);
		this.actions.put(ActionName.PLACE_CITY, 5);
	}

	public Card getCard() {
		int rand;
		Card card = null;
		while (card == null) {
			rand = random.nextInt() % 42;
			if (rand == 0 && doubleCards > 0) {
				doubleCards--;
				card = new Card(chooseResource(), 1, CardType.DOUBLE,
						ActionName.PLACE_ARMY, 1, ActionName.DESTROY_ARMY, 1);
			} else if (0 < rand && rand < 6 && toChooseCards > 0) {
				toChooseCards--;
				ActionName firstAction = chooseAction();
				ActionName secondAction = chooseAction();
				while(firstAction.equals(secondAction)) {
					secondAction = chooseAction();
				}
				card = new Card(chooseResource(), 1, CardType.TO_CHOOSE, firstAction, chooseActionCredits(firstAction), secondAction, chooseActionCredits(secondAction));
			} else {
				ResourceName resource = chooseResource();
				ActionName action = chooseAction();
				card = new Card(resource, chooseResourceNumber(resource),
						action, chooseActionCredits(action));
			}
		}
		return card;
	}

	private ResourceName chooseResource() {
		int rand;
		boolean cardChoosen = false;
		ResourceName resource = null;
		while (cardChoosen == false) {
			rand = random.nextInt() % 6;
			if (rand == 0 && numOfCards.get(ResourceName.ALL) > 0) {
				numOfCards.put(ResourceName.ALL,
						numOfCards.get(ResourceName.ALL) - 1);
				cardChoosen = true;
				resource = ResourceName.ALL;
			} else if (rand == 1 && numOfCards.get(ResourceName.CARROT) > 0) {
				numOfCards.put(ResourceName.CARROT,
						numOfCards.get(ResourceName.CARROT) - 1);
				cardChoosen = true;
				resource = ResourceName.CARROT;
			} else if (rand == 2 && numOfCards.get(ResourceName.COAL) > 0) {
				numOfCards.put(ResourceName.COAL,
						numOfCards.get(ResourceName.COAL) - 1);
				cardChoosen = true;
				resource = ResourceName.COAL;
			} else if (rand == 3 && numOfCards.get(ResourceName.RUBIN) > 0) {
				numOfCards.put(ResourceName.RUBIN,
						numOfCards.get(ResourceName.RUBIN) - 1);
				cardChoosen = true;
				resource = ResourceName.RUBIN;
			} else if (rand == 4 && numOfCards.get(ResourceName.STEEL) > 0) {
				numOfCards.put(ResourceName.STEEL,
						numOfCards.get(ResourceName.STEEL) - 1);
				cardChoosen = true;
				resource = ResourceName.STEEL;
			} else if (rand == 5 && numOfCards.get(ResourceName.WOOD) > 0) {
				numOfCards.put(ResourceName.WOOD,
						numOfCards.get(ResourceName.WOOD) - 1);
				cardChoosen = true;
				resource = ResourceName.WOOD;
			}
		}
		return resource;
	}

	private int chooseResourceNumber(ResourceName resource) {
		if (ResourceName.CARROT.equals(resource)
				&& numOfCards.get(ResourceName.CARROT) == 5) {
			return 2;
		} else if (ResourceName.STEEL.equals(resource)
				&& numOfCards.get(ResourceName.STEEL) == 4) {
			return 2;
		}

		return 1;
	}

	private ActionName chooseAction() {
		int rand;
		boolean actionChoosen = false;
		ActionName action = null;
		while(actionChoosen == false) {
			rand = random.nextInt() % 5;
			if(rand == 0 && actions.get(ActionName.DESTROY_ARMY) > 0) {
				action = ActionName.DESTROY_ARMY;
				actions.put(action, actions.get(action) - 1);
				actionChoosen = true;
			} else if(rand == 1 && actions.get(ActionName.MOVE_ARMY) > 0) {
				action = ActionName.MOVE_ARMY;
				actions.put(action, actions.get(action) - 1);
				actionChoosen = true;
			} else if(rand == 2 && actions.get(ActionName.MOVE_ARMY_BY_SEA) > 0) {
				action = ActionName.MOVE_ARMY_BY_SEA;
				actions.put(action, actions.get(action) - 1);
				actionChoosen = true;
			} else if(rand == 3 && actions.get(ActionName.PLACE_ARMY) > 0) {
				action = ActionName.PLACE_ARMY;
				actions.put(action, actions.get(action) - 1);
				actionChoosen = true;
			} else if(rand == 4 && actions.get(ActionName.PLACE_CITY) > 0) {
				action = ActionName.PLACE_CITY;
				actions.put(action, actions.get(action) - 1);
				actionChoosen = true;
			}
		}
		return action;
	}

	private int chooseActionCredits(ActionName action) {
		int rand;
		if(ActionName.DESTROY_ARMY.equals(action) || ActionName.PLACE_CITY.equals(action)) {
			return 1;
		} else if(ActionName.MOVE_ARMY.equals(action)) {
			rand = random.nextInt() % 14;
			if(rand < 3) {
				return 2;
			} else if(rand< 6) {
				return 3;
			} else if (rand < 11) {
				return 4;
			} else if(rand < 13) {
				return 5;
			}
			return 6;
		} else if(ActionName.MOVE_ARMY_BY_SEA.equals(action)) {
			rand = random.nextInt() % 9;
			if(rand < 4) {
				return 2;
			} else if(rand < 8) {
				return 3;
			}
			return 4;
		} else if(ActionName.PLACE_ARMY.equals(action)) {
			rand = random.nextInt() % 17;
			if(rand < 1) {
				return 4;
			} else if(rand < 7) {
				return 2;
			} else if(rand < 16) {
				return 3;
			}
			return 1;
		}
		return 0;
	}
}
