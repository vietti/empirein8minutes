package controller;

import java.util.Map;

import model.Game;
import model.dto.ActionName;
import model.dto.Army;
import model.dto.Card;
import model.dto.CardType;
import model.dto.Territory;
import view.GameView;

public class GameController {

	Game gameModel;
	GameView gameView;

	public GameController(Game game, GameView view) {
		this.gameModel = game;
		this.gameView = view;
	}

	public void main() {
		startGame();
		getBids();
		while (!gameModel.isGameFinished()) {
			getTurn();
		}

	}

	private void getTurn() {
		int cardPosition = gameView.getCard();
		gameModel.chooseCard(cardPosition);
		Card card = gameModel.getCurrentCard();
		if (CardType.TO_CHOOSE.equals(card.getType())) {
			ActionName action = gameView.getAction();
			gameModel.chooseAction(action);
		}
		while (gameModel.getCredits() != 0) {
			if (ActionName.MOVE_ARMY.equals(card.getActionName())
					|| ActionName.MOVE_ARMY_BY_SEA.equals(card.getActionName())) {
				Army army = gameView.getArmy();
				gameModel.chooseArmy(army);
				Territory territoryToMove = gameView.getTerritory();
				gameModel.chooseTerritory(territoryToMove);
			} else if (ActionName.PLACE_ARMY.equals(card.getActionName())
					|| ActionName.PLACE_CITY.equals(card.getActionName())) {
				Territory territory = gameView.getTerritory();
				gameModel.chooseTerritory(territory);
			} else if (ActionName.DESTROY_ARMY.equals(card.getActionName())) {
				Army army = gameView.getArmy();
				gameModel.chooseArmy(army);
			}
		}
	}

	private void getBids() {
		while (gameModel.isEveryoneBid() == false) {
			Map.Entry<String, Integer> player = gameView.getBid();
			gameModel.bid(player.getKey(), player.getValue());
		}
	}

	private void startGame() {
		int numberOfPlayers = gameView.getNumberOfPlayers();
		gameModel.defineNumbersOfPlayers(numberOfPlayers);
	}
}
